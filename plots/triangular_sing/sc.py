import numpy as np
import matplotlib.pyplot as plt
import h5py
import os
import sys
import numpy as np
import glob
import matplotlib.pyplot as plt
import matplotlib.colors as mpc
import matplotlib.patches as mpp
import matplotlib
import numpy.ma as npma

sys.path.append(os.path.join(os.path.dirname(__file__), '..'))
import colors
import header
import triangular

matplotlib.rc('font', **{'family': 'serif', 'serif': ['Computer Modern Roman'],
                         'size': 8})
matplotlib.rc('text', usetex=True)
matplotlib.rc('text.latex', preamble=r'\usepackage{amsmath}'
                + r'\usepackage{braket}' + r'\usepackage{amssymb}'
                + r'\newcommand{\bvec}[1]{\boldsymbol{#1}}')

fig_width = 1.0 * 7.05687
# fig_width = 3.4039

sig = np.array([[[1, 0], [0, 1]], [[0, 1], [1, 0]],
                [[0, -1j], [1j, 0]], [[1, 0], [0, -1]]])

def savitzky_golay(y, window_size, order, deriv=0, rate=1):
    from math import factorial

    if window_size % 2 != 1 or window_size < 1:
        raise TypeError("window_size size must be a positive odd number")
    if window_size < order + 2:
        raise TypeError("window_size is too small for the polynomials order")
    order_range = range(order + 1)
    half_window = (window_size - 1) // 2
    # precompute coefficients
    b = np.mat([[k**i for i in order_range]
                for k in range(-half_window, half_window + 1)])
    m = np.linalg.pinv(b).A[deriv] * rate**deriv * factorial(deriv)
    # pad the signal at the extremes with
    # values taken from the signal itself
    firstvals = y[0] - np.abs(y[1:half_window + 1][::-1] - y[0])
    lastvals = y[-1] + np.abs(y[-half_window - 1:-1][::-1] - y[-1])
    y = np.concatenate((firstvals, y, lastvals))
    return np.convolve(m[::-1], y, mode='valid')

upper_vH = np.array([
    [0.24921465968586393, 0.42439024390243907],
    [0.25130890052356025, 0.4185365853658537],
    [0.27434554973821984, 0.35414634146341467],
    [0.29424083769633513, 0.3092682926829269],
    [0.3172774869109947, 0.2751219512195122],
    [0.3413612565445026, 0.24878048780487805],
    [0.3633507853403141, 0.22634146341463418],
    [0.3832460732984293, 0.2078048780487805],
    [0.4020942408376963, 0.19219512195121957],
    [0.418848167539267, 0.17853658536585365],
    [0.43246073298429333, 0.16487804878048784],
    [0.4450261780104713, 0.15414634146341472],
    [0.45549738219895286, 0.1453658536585366]])

lower_vH = np.array([
    [0.24921465968586393, 0.42536585365853663],
    [0.24502617801047116, 0.4204878048780488],
    [0.2418848167539267, 0.4],
    [0.23350785340314134, 0.38634146341463416],
    [0.22722513089005242, 0.3746341463414634],
    [0.22094240837696338, 0.3619512195121951],
    [0.2178010471204188, 0.3502439024390244],
    [0.21465968586387435, 0.33951219512195124],
    [0.20942408376963356, 0.33073170731707324],
    [0.2010471204188481, 0.3336585365853659],
    [0.1968586387434555, 0.3678048780487805],
    [0.18219895287958116, 0.40390243902439027],
    [0.16335078534031416, 0.3590243902439024]])


def get_bz_pts(basis, kmesh):
    border_points = []

    def backfold(basis, Qs):
        def backfold_to_first_BZ(basis, Q):
            Q_out = Q
            overflow = []
            d = np.linalg.norm(Q)
            for nn in [-1, 0, 1]:
                for mm in [-1, 0, 1]:
                    Q_offset = Q + mm * basis[0] + nn * basis[1]
                    d_new = np.linalg.norm(Q_offset)
                    if d_new < d:
                        d = d_new
                        Q_out = Q_offset

            return Q_out

        out = np.array([backfold_to_first_BZ(basis, qq) for qq in Qs])
        return out

    bz_pts = backfold(basis, kmesh)

    return bz_pts


def plot_hexagon(ax, b1, b2, color=colors.gray):
    G = np.zeros(2)

    M = [np.zeros(2)] * 6
    K = [np.zeros(2)] * 6

    norm = np.linalg.norm(b1 / 2)

    M[0] = b1 / 2
    M[1] = np.array([0, +norm])
    M[2] = b2 / 2
    M[3] = -b1 / 2
    M[4] = -b2 / 2
    M[5] = np.array([0, -norm])

    K[0] = - b1 / 3 + 1 * b2 / 3
    K[1] = + b1 / 3 + 2 * b2 / 3
    K[2] = + b2 / 3 + 2 * b1 / 3
    K[3] = -K[0]
    K[4] = -K[1]
    K[5] = -K[2]

    ax.plot([K[0][0], K[1][0], K[2][0], K[3][0], K[4][0], K[5][0], K[0][0]],
            [K[0][1], K[1][1], K[2][1], K[3][1], K[4][1], K[5][1], K[0][1]],
            color=color)


alphas = np.arange(0.0, 1.21, 0.05)
fills = np.arange(0.150, 0.601, 0.025)

dfills = fills[1] - fills[0]
dalphas = alphas[1] - alphas[0]

custom_colors = {
    'red': [(0.0, colors.third[0], colors.third[0]),
            (1, colors.first[0], colors.first[0])],
    'green': [(0.0, colors.third[1], colors.third[1]),
              (1, colors.first[1], colors.first[1])],
    'blue': [(0.0, colors.third[2], colors.third[2]),
             (1, colors.first[2], colors.first[2])]
}

cmap = mpc.LinearSegmentedColormap('CustomColormap', custom_colors)

phase = np.load('phase.npy')
sing = np.load("sing.npy")

cut = 11
alphas = alphas[:cut]
sing = sing[:, :cut]
phase = phase[:, :cut]

phase = np.array(phase, dtype='float')

fig, axs = plt.subplot_mosaic([['SC', 'space1', 'cbar', 'space',
                                    'S1', 'T11', 'space2', 'S2', 'T21'],
                               ['SC', 'space1', 'cbar', 'space',
                                    'T12', 'T13', 'space2', 'T22', 'T23']],
                              gridspec_kw={'width_ratios': [4, 0.4,
                                                            0.5, 0.55,
                                                            2, 2, 0.2, 2, 2]},
                              figsize=(fig_width, 0.40 * fig_width))
axs['space'].axis('off')
axs['space2'].axis('off')
axs['space1'].axis('off')

image_kw = dict(extent=(fills[0] - 0.5 * dfills, fills[-1] + 0.5 * dfills,
                        alphas[0] - 0.5 * dalphas, alphas[-1] + 0.5 * dalphas))

im = axs['SC'].imshow(sing[:, ::-1].T, **image_kw, cmap=cmap, vmin=0, vmax=1,
                      aspect='auto')
axs['SC'].set_ylim([np.min(alphas) - 0.02, np.max(alphas) + 0.02])
axs['SC'].set_xlabel(r'$\nu$', labelpad=1)

axs['SC'].set_xlim(0.15 - 2 * dfills, 0.60 + 2 * dfills)
axs['SC'].set_ylabel(r'$\alpha$', labelpad=3)

phase[np.where(phase != 1)] = np.nan
axs['SC'].imshow(phase[:, ::-1].T, vmin=0, vmax=2, cmap='gray', **image_kw,
                 alpha=0.7)
# axs['SC'].text(0.28, 0.45, 'magnetic' + '\n' + 'order', #'\slshape{}(i)SDW',
#                ha='center', va='center',
#                rotation=90)

alphas_ = np.arange(0, 1.21, 0.1)
# Plot the van Hove singularity line
yhat = savitzky_golay(upper_vH[:, 0], 7, 3)
axs['SC'].plot(yhat, alphas_, linestyle='--', color='gray')

yhat = savitzky_golay(lower_vH[:, 0], 7, 3)
axs['SC'].plot(yhat, alphas_, linestyle='--', color='gray')

cb = plt.colorbar(im, cax=axs['cbar'], fraction=0.046, pad=0.4)
cb.set_ticks([0, 1])
cb.set_label('singlet weight', labelpad=-8)
axs['cbar'].yaxis.set_ticks_position('left')
axs['cbar'].yaxis.set_label_position('left')

cax_right = axs['cbar'].twinx()
cax_right.set_yticks([0, 1])
cax_right.set_yticklabels([r'$-$', r'$+$'])
cax_right.set_ylabel(r'Eigenvector', rotation=270, labelpad=-2)

# Generate Eigenvectors
n = 0.2
alpha = 0.1

marker = mpp.Circle((n - 0.01, alpha), radius=0.025, edgecolor='k',
                    facecolor='none', linewidth=0.5)
axs['SC'].add_patch(marker)

# Load vertex and diagonalize V_P at Q=0
mesh_file = h5py.File('mesh.h5')
ff_file = h5py.File('ff.h5')
vertex_file = h5py.File('./vertex_n0.2_a0.1.h5')

mesh = mesh_file['mesh']
nq = len(mesh)
nff = ff_file['nff'][0]
no = 2

# Obtain Q=0 point
Q_zero = np.where(np.isclose(np.sum(mesh, axis=1), 0))

V_P = vertex_file['P']

# Obtain Q=0 subspace
V_P = np.reshape(V_P, (nq, nff, nff, no, no, no, no))
V_P = V_P[Q_zero].reshape((nff, nff, no, no, no, no))

# Obtain Matrix representation of V_P
V_P = np.einsum('fg abcd -> fab gcd', V_P).reshape((nff * no**2, nff * no**2))

evals, evec = np.linalg.eigh(V_P)

deg = 0
for ii in range(10):
    if np.isclose(evals[ii], evals[0]):
        deg += 1

pts = []

basis = header.get_basis(mesh_file)

mesh_bf = get_bz_pts(basis, mesh[:, :2])

ff = np.zeros((nff, nq), dtype=complex)
for ii in range(nff):
    ff[ii, :] = np.array(ff_file['ff' + str(ii)]) * 1. / np.sqrt(nq)


for ii in range(deg):
    temp = evec[:, ii].reshape(nff, no, no)
    temp_st = np.einsum('fij, il, alj -> fa', temp,
                        1j / np.sqrt(2) * sig[2], sig)

    ff_st = np.einsum('fk, fa -> ka', ff, temp_st)

    pts.append(ff_st)


set1 = ['S1', 'T11', 'T12', 'T13']
set2 = ['S2', 'T21', 'T22', 'T23']

text = [r'$\Psi$', r'$\boldsymbol d_x$', r'$\boldsymbol d_y$',
        r'$\boldsymbol d_z$']

c1 = [1, 1, 1]
c2 = colors.second * 0.6
ev_colors = {
    'red': [(0.0, c1[0], c1[0]),
            (1.0, c2[0], c2[0])],
    'green': [(0.0, c1[1], c1[1]),
              (1.0, c2[1], c2[1])],
    'blue': [(0.0, c1[2], c1[2]),
             (1.0, c2[2], c2[2])]
}

vmax = np.max([np.max(pts[0].flatten().real), np.min(pts[0].flatten().imag),
               np.max(pts[1].flatten().real), np.max(pts[1].flatten().imag)])

ev_cmap = mpc.LinearSegmentedColormap('CustomColormap', ev_colors)

for ii, nn in enumerate(set1):
    ax = axs[nn]
    ax.set_aspect('equal')
    plot_hexagon(ax, basis[0], basis[1])
    ax.axis('off')
    ax.tricontourf(mesh_bf[:, 0], mesh_bf[:, 1], np.imag(pts[0][:, ii]), 20,
                   vmin=-vmax, vmax=vmax, cmap=cmap)
    ax.text(-4.5, 2.8, text[ii])

for ii, nn in enumerate(set2):
    ax = axs[nn]
    ax.set_aspect('equal')
    plot_hexagon(ax, basis[0], basis[1])
    ax.axis('off')
    ax.tricontourf(mesh_bf[:, 0], mesh_bf[:, 1], np.real(pts[1][:, ii]), 20,
                   vmin=-vmax, vmax=vmax, cmap=cmap)
    ax.text(-4.5, 2.8, text[ii])

fig.text(0.435, 0.06, r'$\Im(\text{State 1})$')
fig.text(0.72, 0.06, r'$\Re(\text{State 2})$')

plt.subplots_adjust(wspace=0.05, hspace=0.0)

fig.savefig('../sc_inst.pdf', bbox_inches="tight")
