import numpy as np
import h5py

sig = np.array([[[1, 0], [0, 1]], [[0, 1], [1, 0]],
                [[0, -1j], [1j, 0]], [[1, 0], [0, -1]]])


def get_basis(file, fine=False):
    if False:
        return np.array([np.array(file["fine_G1"])[:2, 0],
                         np.array(file["fine_G2"])[:2, 0]])

    return np.array([np.array(file["mesh_G1"])[:2, 0],
                     np.array(file["mesh_G2"])[:2, 0]])


def make_mesh(n, mesh_basis):
    k_spacings = np.linspace(-0.5, 0.5, n + 1)[:-1]
    mesh_grid = np.array([np.tile(k_spacings, n), np.repeat(k_spacings, n)]).T

    lin_kmesh = np.einsum("ix, xl -> il", mesh_grid, mesh_basis)
    return lin_kmesh
