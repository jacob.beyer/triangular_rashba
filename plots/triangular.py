import numpy as np
import sys
import os

from header import sig


def get_hsp(basis=[[6.28318531, -3.62759873], [0., 7.25519746]]):
    b1 = np.array(basis[0])
    b2 = np.array(basis[1])

    G = [np.zeros(2)]

    M = [np.zeros(2)] * 6
    K = [np.zeros(2)] * 6

    norm = np.linalg.norm(b1 / 2)

    M[0] = b1 / 2
    M[1] = np.array([0, +norm])
    M[2] = b2 / 2
    M[3] = -b1 / 2
    M[4] = -b2 / 2
    M[5] = np.array([0, -norm])

    K[0] = - b1 / 3 + 1 * b2 / 3
    K[1] = + b1 / 3 + 2 * b2 / 3
    K[2] = + b2 / 3 + 2 * b1 / 3
    K[3] = -K[0]
    K[4] = -K[1]
    K[5] = -K[2]

    return G, K, M


def dispersion(k, t, a1, a2, a3):
    e = t[0] \
        + t[1] * 2 * (np.cos(np.dot(a1, k))
                      + np.cos(np.dot(a2, k))
                      + np.cos(np.dot(a3, k))) \
        + t[2] * 2 * (np.cos(np.dot(1. * a1 + a2, k))
                      + np.cos(np.dot(1. * a2 + a3, k))
                      + np.cos(np.dot(1. * a3 - a1, k))) \
        + t[3] * 2 * (np.cos(np.dot(2. * a1, k))
                      + np.cos(np.dot(2. * a2, k))
                      + np.cos(np.dot(2. * a3, k))) \
        + t[4] * 2 * (np.cos(np.dot(2. * a1 + a2, k))
                      + np.cos(np.dot(2. * a1 - a3, k))
                      + np.cos(np.dot(2. * a2 + a1, k))
                      + np.cos(np.dot(2. * a2 + a3, k))
                      + np.cos(np.dot(2. * a3 + a2, k))
                      + np.cos(np.dot(2. * a3 - a1, k))) \
        + t[5] * 2 * (np.cos(np.dot(2. * a1 + 2. * a2, k))
                      + np.cos(np.dot(2. * a2 + 2. * a3, k))
                      + np.cos(np.dot(2. * a3 - 2. * a1, k))) \
        + t[6] * 2 * (np.cos(np.dot(3. * a1, k))
                      + np.cos(np.dot(3. * a2, k))
                      + np.cos(np.dot(3. * a3, k))) \
        + t[7] * 2 * (np.cos(np.dot(3. * a1 + 1. * a2, k))
                      + np.cos(np.dot(3. * a1 - 1. * a3, k))
                      + np.cos(np.dot(3. * a2 + 1. * a1, k))
                      + np.cos(np.dot(3. * a2 + 1. * a3, k))
                      + np.cos(np.dot(3. * a3 + 1. * a2, k))
                      + np.cos(np.dot(3. * a3 - 1. * a1, k)))
    return e


def a1_rashba_term(k, a1, a2, a3):
    return \
        - 2 * sig[1] * (0.0 * np.sin(np.dot(a1, k))
                        + np.sqrt(3) / 2 * np.sin(np.dot(a2, k))
                        + np.sqrt(3) / 2 * np.sin(np.dot(a3, k))) \
        + 2 * sig[2] * (1.0 * np.sin(np.dot(a1, k))
                        + 0.5 * np.sin(np.dot(a2, k))
                        - 0.5 * np.sin(np.dot(a3, k)))


def a2_rashba_term(k, a1, a2, a3):
    return \
        - 2 * sig[1] * (np.sqrt(3) / 2 * np.sin(np.dot(2. * a1 + a3, k))
                        + 1. * np.sqrt(3) * np.sin(np.dot(1. * a2 + a3, k))
                        + np.sqrt(3) / 2 * np.sin(np.dot(1. * a3 - a1, k))) \
        + 2 * sig[2] * (1.5 * np.sin(np.dot(2. * a1 + a3, k))
                        + 0.0 * np.sin(np.dot(1. * a2 + a3, k))
                        - 1.5 * np.sin(np.dot(1. * a3 - a1, k)))


def h0(k, t, alpha,
       a1=np.array([1, 0, 0]),
       a2=np.array([0.5, np.sqrt(3) / 2, 0]),
       a3=None):
    if a3 is None:
        a3 = a2 - a1

    if len(k) == 2:
        a1 = a1[:2]
        a2 = a2[:2]
        a3 = a3[:2]

    h0 = np.zeros((2, 2), dtype='complex128')
    h0[0, 1] = 0.0
    h0[1, 0] = 0.0
    h0[0, 0] = dispersion(k, t, a1, a2, a3)
    h0[1, 1] = dispersion(k, t, a1, a2, a3)
    h0 += alpha[0] * t[1] * a1_rashba_term(k, a1, a2, a3)
    h0 += alpha[1] * t[2] * a2_rashba_term(k, a1, a2, a3)
    return h0


def plot_hexagon(ax, b1, b2, shift=np.zeros(2), M_lines=False,
                 K_lines=False, color='orange'):
    G = shift

    M = [np.zeros(2)] * 6
    K = [np.zeros(2)] * 6

    norm = np.linalg.norm(b1 / 2)

    M[0] = b1 / 2
    M[1] = np.array([0, +norm])
    M[2] = b2 / 2
    M[3] = -b1 / 2
    M[4] = -b2 / 2
    M[5] = np.array([0, -norm])

    K[0] = - b1 / 3 + 1 * b2 / 3
    K[1] = + b1 / 3 + 2 * b2 / 3
    K[2] = + b2 / 3 + 2 * b1 / 3
    K[3] = -K[0]
    K[4] = -K[1]
    K[5] = -K[2]

    for i in range(6):
        M[i] += shift
        K[i] += shift

    if M_lines:
        for ii in range(6):
            ax.plot([G[0], M[ii][0]], [G[1], M[ii][1]], color='red')

    if K_lines:
        for ii in range(6):
            ax.plot([G[0], K[ii][0]], [G[1], K[ii][1]], color='blue')

    ax.plot([K[0][0], K[1][0], K[2][0], K[3][0], K[4][0], K[5][0], K[0][0]],
            [K[0][1], K[1][1], K[2][1], K[3][1], K[4][1], K[5][1], K[0][1]],
            color=color)


if __name__ == "__main__":
    E = h0(np.array([1., 0., 0.]), [0, 1.0, 0, 0, 0, 0, 0, 0, 0], [0., 0.])
    print(E)
