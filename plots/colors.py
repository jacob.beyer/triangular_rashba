import numpy as np

first = np.array([0, 153, 230]) / 256
onehalf = np.array([29, 69, 170]) / 256
second = np.array([40, 37, 110]) / 256
third = np.array([242, 56, 20]) / 256

gray = np.array([125, 125, 125]) / 256


def interpolate(i, N, color1=first, color2=third):
    color = color1 * (N - i) / N + color2 * i / N
    return color
