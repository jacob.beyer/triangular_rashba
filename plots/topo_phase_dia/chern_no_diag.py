'''
    Chern number diagram
'''
import sys
import os
import h5py  #type: ignore
import numpy as np
import matplotlib as mpl #type:ignore
import matplotlib.patches as mpatches #type:ignore
import matplotlib.pyplot as plt  #type:ignore

import chern_no as chern
import ff_vec_loader as ff_load

mpl.rc('font', **{'family': 'serif', 'serif': ['Computer Modern Roman'],
                         'size': 8})
mpl.rc('text', usetex=True)
mpl.rc('text.latex', preamble=r'\usepackage{amsmath}'
                + r'\usepackage{braket}' + r'\usepackage{amssymb}'
                + r'\newcommand{\bvec}[1]{\boldsymbol{#1}}')

I2 = np.array([[1,0],[0,1]], dtype=complex)
sx = np.array([[0,1],[1,0]],dtype=complex)
sy = np.array([[0,-1j],[1j,0]],dtype=complex)


def hopping(k, params, su2 = False):
    t = params['t']
    alph = params['alph']
    mu = params['mu']

    a1 = np.array([1,0,0])
    ca1 = np.cos(np.einsum('ki,i->k',k,a1))
    sa1 = np.sin(np.einsum('ki,i->k',k,a1))
    a2 = np.array([1/2,np.sqrt(3)/2,0])
    ca2 = np.cos(np.einsum('ki,i->k',k,a2))
    sa2 = np.sin(np.einsum('ki,i->k',k,a2))
    a3 = a2-a1
    ca3 = np.cos(np.einsum('ki,i->k',k,a3))
    sa3 = np.sin(np.einsum('ki,i->k',k,a3))


    hop =  np.einsum('k,rs->krs', 2*t* (ca1 + ca2 + ca3) + mu, I2)

    if su2 is True:
        return hop[:,0,0]

    hop += -2 * alph * (
        np.einsum('k,rs->krs', a1[1]*sa1 + a2[1]*sa2 + a3[1]*sa3, sx) -\
        np.einsum('k,rs->krs', a1[0]*sa1 + a2[0]*sa2 + a3[0]*sa3, sy))

    return np.conj(hop)


def delta_mat(k, ff_vec, ff_file, su2 = False):
    ff_r = ff_file['real'][:]
    if su2 is True:
        delta = np.einsum('if,f->i',np.exp(-1j*np.einsum('ki,fi->kf',k, ff_r)),ff_vec)
    else:
        delta = np.einsum('if,frs->irs',np.exp(-1j*np.einsum('ki,fi->kf',k, ff_r)),ff_vec)
    # delta_plots.plot_delta(k ,delta)
    return delta

def calculate_chern(data_file, ff):

        n_ff = int(ff['nff'][:])

        alpha_vals = data_file['alpha'][:]
        fill_vals = data_file['filling'][:]
        mu_vals = data_file['mu']
        P_channel_evecs = data_file['P_channel_evecs'][:]
        channel = data_file['channel'][:]
        lamc = data_file['lamc'][:]

        channel[lamc < 1e-4] = 3

        error = 0
        nk_chern = 300
        gap_size = 5e-3
        chern_mesh = chern.setup_mesh(nk_chern)

        chern_no = np.zeros_like(mu_vals)
        gap = np.zeros_like(mu_vals)
        ratios = np.empty((len(mu_vals),3),dtype=complex)
        sing_trip_weights = np.empty_like(mu_vals)

        xfm = ff_load.get_xfm_to_symbasis(n_ff)
        sym_evecs = np.reshape(np.einsum('ij,klj->kli',xfm,P_channel_evecs),(len(alpha_vals),3,n_ff,4))

        unique_alph = np.unique(alpha_vals)
        unique_alph = np.flip(unique_alph[unique_alph <= 0.5])
        unique_fill = np.unique(fill_vals)
        chern_no_grid = np.zeros((len(unique_alph),len(unique_fill)))
        channel_grid = np.zeros_like(chern_no_grid)
        gap_grid = np.zeros_like(chern_no_grid)

        for i, mu in enumerate(mu_vals):

            if alpha_vals[i] > 0.5:
                alph_ind = np.argwhere(unique_alph == alpha_vals[i])
                fill_ind = np.argwhere(unique_fill == fill_vals[i])
                chern_no_grid[alph_ind, fill_ind] = chern_no[i]
                gap_grid[alph_ind, fill_ind] = gap[i]
                continue
            if not channel[i] == 0:
                alph_ind = np.argwhere(unique_alph == alpha_vals[i])
                fill_ind = np.argwhere(unique_fill == fill_vals[i])
                chern_no_grid[alph_ind, fill_ind] = np.nan
                channel_grid[alph_ind, fill_ind] = channel[i]
                continue

            if alpha_vals[i] == 0:
                dz_weight = np.amax(np.abs(sym_evecs[i,:,:,-1]),axis=1)
                ind = np.delete([0,1,2], np.argmax(dz_weight))
            else:
                ind = [0,1]

            # mu_ind = np.argwhere(np.logical_and(alpha_vals == 0, fill_vals == fill_vals[i]))[0,0]

            params = {
                'fil' :fill_vals[i],
                't'   :1,
                'alph':alpha_vals[i],
                'mu'  :-mu
            }
            print(params)

            ff_vec, ratios[i,:] = ff_load.get_ff_vec(
                P_channel_evecs[i,ind[0],:],
                P_channel_evecs[i,ind[1],:],
                n_ff,
                fill_vals[i],
                thetaphi = np.array([np.pi/4, np.pi/2])
            )

            if np.isclose(alpha_vals[i], 0):
                chern_hamil = hopping(chern_mesh, params, su2 = True)

                def chern_bdg_ham(k, ff_vec, ff_file):
                    ham = np.empty((np.shape(k)[0],2,2),dtype=complex)
                    Del = gap_size*delta_mat(k, ff_vec, ff_file, su2 = False)
                    Del = Del[:,0,1]
                    ham[:, 0, 0] = chern_hamil
                    ham[:, 1, 1] = -chern_hamil
                    ham[:, 0, 1] = Del
                    ham[:, 1, 0] = Del.conj()
                    return ham

                chernnos, en, _, _ = chern.calculate_chern_nos(
                    params,ff,np.reshape(ff_vec,(n_ff,2,2)),points=nk_chern,nband = 2,ham_func=chern_bdg_ham)

                try:
                    chern_no[i] = 2*int(np.sum(chernnos[0]))
                except ValueError:
                    error += 1
                    continue
                gap[i] = 2*np.amin(np.abs(en))


            else:
                chern_hamil = hopping(chern_mesh, params)
                # chern_hopping_en, _ = linalg.eigh(chern_hamil)
                chern_hole_hamil = np.copy(chern_hamil)
                for k in range(2):
                    chern_hole_hamil[:, k, k] -= 2*chern_hamil[:, k, k]
                chern_hole_hamil = np.swapaxes(chern_hole_hamil, 1, 2)


                def chern_bdg_ham(k, ff_vec, ff_file):
                    ham = np.empty((np.shape(k)[0],4,4),dtype=complex)
                    Del = gap_size*delta_mat(k, ff_vec, ff_file)
                    ham[:, :2, :2] = chern_hamil
                    ham[:, 2:, 2:] = chern_hole_hamil
                    ham[:, :2, 2:] = Del
                    ham[:, 2:, :2] = np.swapaxes(Del.conj(),1,2)
                    return ham

                chernnos, en, _, _ = chern.calculate_chern_nos(
                    params,ff,np.reshape(ff_vec,(n_ff,2,2)),points=nk_chern,ham_func=chern_bdg_ham)

                gap[i] = 2*np.amin(np.abs(en))
                chern_no[i] = int(np.sum(chernnos[:2]))

            alph_ind = np.argwhere(unique_alph == alpha_vals[i])
            fill_ind = np.argwhere(unique_fill == fill_vals[i])
            chern_no_grid[alph_ind, fill_ind] = chern_no[i]
            gap_grid[alph_ind, fill_ind] = gap[i]

        del data_file['channel_grid']
        data_file.create_dataset('channel_grid', data=channel_grid)
        del data_file['chern_no_grid']
        data_file.create_dataset('chern_no_grid', data=chern_no_grid)
        del data_file['gap_grid']
        data_file.create_dataset('gap_grid', data=gap_grid)
        del data_file['chern']
        data_file.create_dataset('chern', data=chern_no)
        del data_file['gap']
        data_file.create_dataset('gap', data=gap)

        print(f'errors: {error}')
        return channel_grid, chern_no_grid, gap_grid


def main():

    dir_path = './'
    ff = h5py.File(dir_path + 'ff.h5', 'r')
    data_file = h5py.File(dir_path +'tri_model_data','r+')

    calculate = False

    if calculate is False:
        chern_no_grid = data_file['chern_no_grid'][:]
        # gap_grid = data_file['gap_grid'][:]
        channel_grid = data_file['channel_grid'][:]

    if calculate is True:
        chern_no_grid, gap_grid, channel_grid = calculate_chern(data_file, ff)

    data_file.close()
    ff.close()


    fig_width = 3.4039
    right_ax_factor = 0.25
    fig_height = fig_width

    fig, ax = plt.subplots(figsize=(fig_width/(1+right_ax_factor), fig_height))
    cmap_name = "tab10"
    cmap_cols_ = list(mpl.colormaps[cmap_name].colors[:4])
    cmap_cols_.insert(0, (0,0,0))
    cmap_cols_[2], cmap_cols_[1] = cmap_cols_[1], cmap_cols_[2]
    cmap_cols = tuple(cmap_cols_)
    cmap = mpl.colors.ListedColormap(cmap_cols)
    ax.imshow(channel_grid == 1, vmin=0, vmax=2, cmap='gray_r', alpha=0.7)
    ax.imshow(np.abs(chern_no_grid),cmap=cmap)
    ax.text(4.5,5,'magnetic\norder', rotation=90, horizontalalignment='center',
        verticalalignment='center',)

    # p0 = mpatches.Patch(color=cmap_cols[0], label=r'$0$')
    # p2 = mpatches.Patch(color=cmap_cols[1], label=r'$2$')
    # p4 = mpatches.Patch(color=cmap_cols[2], label=r'$4$')
    # p6 = mpatches.Patch(color=cmap_cols[3], label=r'$-6$')
    # p8 = mpatches.Patch(color=cmap_cols[4], label=r'$-8$')
    # plt.legend(handles=[p0, p2, p4, p6, p8],loc='lower right')

    fxm = lambda x: 40*x-6
    axm = lambda x: 10-20*x
    # ax.text(fxm(0.175),axm(0),'gapless',va='center',ha='center')
    ax.text(fxm(0.1875),axm(0.125),'4',va='center',ha='center')
    ax.text(fxm(0.38),axm(0.1),r'$-8$',va='center',ha='center')
    ax.text(fxm(0.525),axm(0.25),'4',va='center',ha='center')
    ax.text(fxm(0.38),axm(0.3125),r'$-6$',va='center',ha='center')
    ax.text(fxm(0.35),axm(0.45),r'$-2$',va='center',ha='center')
    ax.text(fxm(0.325),axm(0.3125),r'$-2$',va='center',ha='center')


    ax.set_aspect(1)
    ax.set_xlabel(r'$\nu$', labelpad=-2)
    ax.set_ylabel(r'$\alpha/t$', labelpad=0)
    ax.set_yticks(np.arange(0,12,2), np.round(np.flip(np.arange(0,0.6,0.1)),2))
    ax.set_xticks(2+np.arange(0,20,4), np.round((np.arange(0.2,0.7,0.1)),2))

    ax.text(fxm(0.1375) + 0.18, (axm(0.525))+0.18, '(a)', ha='left', va='top')


    min_gap = np.load('./min_gap.npy')
    gap_zeros = np.load('./gap_zeros.npy')


    data_file = h5py.File('./data_combo')
    alpha_vals = data_file['alpha'][:]

    mask = np.logical_and(0.175 <= alpha_vals, alpha_vals <= 0.5)
    min_alph, max_alph = 0.11, 0.325
    max_delt = 10**(1.1*np.log10(gap_zeros[np.argmin(np.abs(alpha_vals - max_alph))]))

    ax1 = ax.inset_axes([1.06,0,right_ax_factor,1])
    ax1.yaxis.tick_right()

    alpha_to_plot = np.sort(alpha_vals[mask])
    gap_to_plot = gap_zeros[mask][np.argsort(alpha_vals[mask])]
    ax1.plot(alpha_to_plot,gap_to_plot,'k.', markersize=1.5)
    ax1.axvspan(min_alph, 0.185, color=cmap_cols_[-1], linewidth=0.0)
    ax1.fill_between(alpha_to_plot,np.zeros_like(alpha_to_plot),gap_to_plot, color=cmap_cols_[-2], linewidth=0.1)
    ax1.fill_between(alpha_to_plot,gap_to_plot, np.ones_like(alpha_to_plot)*max_delt, color=cmap_cols_[-1], linewidth=0)



    # ax1.axvspan(min_alph, 0.17, alpha=0.5, color=mpl.colormaps['gray'](0.5), linewidth=0.0)
    # ax1.text((0.17+min_alph)/2, max_delt/2,'no transition', rotation=90, horizontalalignment='center',
    #     verticalalignment='center',)
    ax1.text(min_alph + 0.15*(max_alph - min_alph), 0.8*max_delt, r'$-8$', ha='left', va='top')
    ax1.text(max_alph - 0.2*(max_alph - min_alph), 0.3*max_delt, r'$-6$', ha='right', va='top')
    ax1.text(min_alph + 0.04*(max_alph - min_alph), 0.98*max_delt, '(b)', ha='left', va='top')
    # ax1.text(max_alph - 0.5*(max_alph - min_alph), 0.01*max_delt, r'$\nu = 0.4$', ha='center', va='bottom')


    ax1.set_xlim([min_alph,max_alph])
    ax1.set_ylim([0,max_delt])
    ax1.set_xlabel(r'$\alpha/t$', labelpad=-2)
    ax1.yaxis.set_label_position("right")
    ax1.set_ylabel(r'$|\Delta|/t$', labelpad=1)
    plt.savefig(dir_path + '/chern_no_diagram.png', bbox_inches='tight', pad_inches=0.01, dpi=700)
    plt.close()

    # fig, ax = plt.subplots()
    # gap_grid[np.isclose(gap_grid,0)] = np.nan
    # # im = ax.scatter(fill_vals[mask],alpha_vals[mask], c=np.log10(gap[mask]), marker='s',s=200, label=c)
    # im = ax.imshow(np.log10(gap_grid))
    # cbar = plt.colorbar(im)
    # cbar.set_label(r'min $E$')
    # # ax.set_aspect(1)
    # ax.set_xlabel(r'$\nu$')
    # ax.set_ylabel(r'$\alpha$')
    # # ax.set_ylim([0,0.6])
    # plt.savefig(dir_path + '/bdg_gap.png', bbox_inches='tight', pad_inches=0.01, dpi=700)
    # plt.close()









if __name__ == "__main__":
    main()
