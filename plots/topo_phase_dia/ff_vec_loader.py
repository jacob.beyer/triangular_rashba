import numpy as np
import h5py  #type: ignore
from numpy import linalg


def load_vertex(ii, ddir):
    f = h5py.File(ddir + 'vertex.h5', 'r')
    keys = ['P', 'C', 'D']
    chi = f[keys[ii]]
    return np.array(chi, dtype=complex)


def reshape_vertex(X, ii, ddir):
    # Parameters
    # Number of Orbitals
    output = h5py.File(ddir + "output.h5", "r")
    n_orb = int(output['nband'][:])
    # n_orb = 2
    # Red. BZ form factors
    ff = h5py.File(ddir + "ff.h5", "r")
    n_ff = int(ff['nff'][:])
    # Bosonic Mesh
    mesh = h5py.File(ddir + "mesh.h5", "r")
    nkpt = int(mesh['mesh_nk'][0])

    # reshape from 4D to 6D tensor
    Xp = np.reshape(X, (nkpt, n_ff, n_ff, n_orb, n_orb, n_orb, n_orb))

    # reorder then reshape to 2D tensors
    mat = n_ff * n_orb**2
    order = [
        "Q fg spqr -> Q fsp gqr",
        "Q fg spqr -> Q fsr gqp",
        "Q fg spqr -> Q fsq grp"
    ]
    # return np.reshape(Xp, (nkpt, mat, mat))
    return np.reshape(np.einsum(order[ii], Xp), (nkpt, mat, mat))


def sym_one_shell_six():
    u_mat = np.zeros((6, 6))
    u_mat[0, :] = 1/np.sqrt(6)*np.ones(6) #s
    u_mat[1, :] = 1/np.sqrt(6)*np.array([-1, 1, 1, -1, -1, 1]) #f
    u_mat[2, :] = 1/np.sqrt(12)*np.array([1, 2, -1, 1, -2, -1]) #px
    u_mat[3, :] = 1/np.sqrt(12)*np.array([-1, 2, -1, -1, 2, -1]) #d1
    u_mat[4, :] = 1/2*np.array([1, 0, 1, -1, 0, -1]) #py
    u_mat[5, :] = 1/2*np.array([1, 0, -1, -1, 0, 1]) #d2
    return u_mat


def get_xfm_to_symbasis(n_ff):
    spin_xfm = (1/2)*np.array([
            [0,1,-1,0],
            [-1,0,0,1],
            [-1j,0,0,-1j],
            [0,1,1,0]
        ])
    ff_xfm = np.zeros((n_ff, n_ff))
    ff_xfm[0,0] = 1
    ff_xfm[1:7,1:7] = sym_one_shell_six()
    ff_xfm[7:13,7:13] = sym_one_shell_six()
    ff_xfm[13:19,13:19] = sym_one_shell_six()
    return np.kron(ff_xfm, spin_xfm)


def get_ff_vec_from_vertex(dir_path, fil, alpha, thetaphi=np.array([0,0])):
    ff = h5py.File(dir_path + 'ff.h5', 'r')
    n_ff = int(ff['nff'][:])

    V_P = load_vertex(0, dir_path)
    V_P = reshape_vertex(V_P, 0, dir_path)
    evals, evecs = linalg.eigh(V_P)

    if alpha == 0:
        xfm = get_xfm_to_symbasis(n_ff)
        sym_evecs = np.reshape(np.einsum('ij,jk->ik',xfm,evecs[:,:3]),(n_ff,4,3))
        dz_weight = np.amax(np.abs(sym_evecs[:,-1,:]),axis=1)
        evec_ind = np.delete([0,1,2], np.argmax(dz_weight))
    else:
        evec_ind = [0,1]

    ff_vec_0 = evecs[np.argmin(evals[:,0]),:,evec_ind[0]]
    ff_vec_1 = evecs[np.argmin(evals[:,0]),:,evec_ind[1]]

    return get_ff_vec(ff_vec_0, ff_vec_1, n_ff, fil, thetaphi)

def get_vecs_from_vertex(dir_path, fil, alpha):
    ff = h5py.File(dir_path + 'ff.h5', 'r')
    n_ff = int(ff['nff'][:])

    V_P = load_vertex(0, dir_path)
    V_P = reshape_vertex(V_P, 0, dir_path)
    evals, evecs = linalg.eigh(V_P)

    if alpha == 0:
        xfm = get_xfm_to_symbasis(n_ff)
        sym_evecs = np.reshape(np.einsum('ij,jk->ik',xfm,evecs[:,:3]),(n_ff,4,3))
        dz_weight = np.amax(np.abs(sym_evecs[:,-1,:]),axis=1)
        evec_ind = np.delete([0,1,2], np.argmax(dz_weight))
    else:
        evec_ind = [0,1]

    ff_vec_0 = evecs[np.argmin(evals[:,0]),:,evec_ind[0]]
    ff_vec_1 = evecs[np.argmin(evals[:,0]),:,evec_ind[1]]

    dx_vec, _ = get_ff_vec(ff_vec_0, ff_vec_1, n_ff, fil, np.array([0,0]))
    dy_vec, _ = get_ff_vec(ff_vec_0, ff_vec_1, n_ff, fil, np.array([np.pi/2,0]))
    return dx_vec, dy_vec


def get_ff_vec(ff_vec_0,ff_vec_1, n_ff, fil, thetaphi=np.array([0,0])):

    xfm = get_xfm_to_symbasis(n_ff)

    sym_vec_0 = np.reshape(np.einsum('ij,j->i',xfm,ff_vec_0),(n_ff,4))
    sym_vec_1 = np.reshape(np.einsum('ij,j->i',xfm,ff_vec_1),(n_ff,4))

    if fil > 0.3:
        #fill > 0.3: set the phase by the largest d-wave component
        ind0 = (np.argmax(np.abs(sym_vec_0[:,0])),0)
        ind1 = (np.argmax(np.abs(sym_vec_1[:,0])),0)
    else:
        #fill < 0.3: set the phase by the NN shell f-wave dx and dy components
        ind0 = (3,1)
        ind1 = (3,2)

    fdx_vec = sym_vec_1[ind1[0],ind1[1]]*ff_vec_0 - sym_vec_0[ind1[0],ind1[1]]*ff_vec_1
    fdy_vec = sym_vec_1[ind0[0],ind0[1]]*ff_vec_0 - sym_vec_0[ind0[0],ind0[1]]*ff_vec_1

    sym_fdx = np.reshape(np.einsum('ij,j->i',xfm,fdx_vec),(n_ff,4))
    sym_fdy = np.reshape(np.einsum('ij,j->i',xfm,fdy_vec),(n_ff,4))

    if False:
        sym_fdx_ = np.zeros_like(sym_fdx)
        sym_fdy_ = np.zeros_like(sym_fdx)
        list_to_copy = [3,5,9,11,15,17] #[2,8,14]
        for ind in list_to_copy:
            sym_fdx_[ind,:] = sym_fdx[ind,:]
            sym_fdy_[ind,:] = sym_fdy[ind,:]


        fdx_vec = np.einsum('ij,j->i',linalg.inv(xfm),np.reshape(sym_fdx_,(n_ff*4)))
        fdy_vec = np.einsum('ij,j->i',linalg.inv(xfm),np.reshape(sym_fdy_,(n_ff*4)))


    fdx_vec = fdx_vec*np.exp(-1j*np.angle(sym_fdx[ind0[0],ind0[1]]))/np.sqrt(np.vdot(fdx_vec,fdx_vec))
    fdy_vec = fdy_vec*np.exp(-1j*np.angle(sym_fdy[ind1[0],ind1[1]]))/(np.sqrt(np.vdot(fdy_vec,fdy_vec)))


    sym_fdx = np.reshape(np.einsum('ij,j->i',xfm,fdx_vec),(n_ff,4))
    sym_fdy = np.reshape(np.einsum('ij,j->i',xfm,fdy_vec),(n_ff,4))

    ratios = get_state_ratios(sym_fdx, ind0, fil)

    ff_vec = np.cos(thetaphi[0])*fdx_vec + np.sin(thetaphi[0])*np.exp(1j*thetaphi[1])*fdy_vec

    sym_ff = np.reshape(np.einsum('ij,j->i',xfm,ff_vec),(n_ff,4))

    # print(np.round(sym_ff,2))
    # input()

    return ff_vec, ratios


def get_state_ratios(sym_fdx, ind0, fil):
    ratios = np.empty(3,dtype=complex)

    if fil > 0.3:
        ratios[0] =  sym_fdx[ind0[0],ind0[1]] #d-wave
        if ind0[0] == 4:
            ratios[1] = sym_fdx[2,2] #f-wave
            ratios[2] = sym_fdx[3,2] #p-wave
        if ind0[0] == 6:
            ratios[1] = sym_fdx[2,1] #f-wave
            ratios[2] = sym_fdx[3,1] #f-wave
        if ind0[0] == 10:
            ratios[1] = sym_fdx[8,1] #f-wave
            ratios[2] = sym_fdx[9,1] #p-wave
        if ind0[0] == 12:
            ratios[1] = sym_fdx[8,2] #f-wave
            ratios[2] = sym_fdx[9,2] #p-wave
    else:
        ratios[0] = sym_fdx[4,0] #d-wave
        ratios[1] = sym_fdx[2,1] #f-wave
        ratios[2] = sym_fdx[3,1] #p-wave

    # print(np.round(ratios[:],2))

    return ratios
