#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import numpy as np
import matplotlib.pyplot as plt #type: ignore
from matplotlib import rc  #type: ignore
import h5py #type:ignore

# import periodic_hamiltonian as pham
import ff_vec_loader as ff_load

rc('font', **{'family': 'serif', 'serif': ['Computer Modern']})
rc('text', usetex=True)


def plot_2D(kx,ky,arry_to_plot,cbar_label,vmax=None,ax_lab=None,cmap='plasma'):
    fig, ax = plt.subplots(1,2)

    vmin = -np.amax(np.abs(arry_to_plot))
    if vmax is None:
        vmax = np.amax(arry_to_plot)

    def plot_on_ax(ax,arry,name):
        im = ax.pcolormesh(kx, ky,
                           arry,cmap=cmap, vmin = vmin, vmax=vmax)
        ax.set_aspect('equal')
        ax.title.set_text(name)
        ax.set_ylabel(r'$k_y$')
        ax.set_xlabel(r'$k_x$')
        return im

    for i in range(2):
        if ax_lab is None:
            name = rf'$n={i+1}$'
        else:
            name = rf'$n={i+1}$' + '\n' + rf'$c_n={int(ax_lab[i])}$'
        im  = plot_on_ax(ax[i], arry_to_plot[:,:,0],name)

    fig.tight_layout()
    # fig.text(0.5,0.88,"Energy Bands",horizontalalignment='center',fontsize=16)
    fig.subplots_adjust(top=0.8, bottom=0.2)
    cbar_ax = fig.add_axes([0.1, 0.1, 0.8, 0.05])
    cbar_ax.set_ylabel(cbar_label,rotation=0, fontsize=12,labelpad=10)
    fig.colorbar(im, cax=cbar_ax, orientation='horizontal')

    return fig


def plot_high_sym(arry, points):
    fig, ax = plt.subplots(2,1)

    def plot_on_ax(ax,arry,name):
        ax.plot(arry)
        ax.title.set_text(name)
        ax.set_ylabel('Flux')
        ax.set_xticks((0,int(np.round(points/3)),
                       int(np.round(points/2)),int(np.round(2*points/3)),points))
        ax.set_xticklabels(('$\Gamma$','$K$','$M$','$K\'$','$\Gamma$'))

    for i in range(2):
        plot_on_ax(ax[i],arry[:,i],rf'$n={i+1}$')

    fig.tight_layout()
    return fig


def setup_mesh(points):
    r_vals = np.arange(points) #+0.01*np.ones(points)
    r1,r2 = np.meshgrid(r_vals,r_vals)
    b1 = np.array([0,4*np.pi/np.sqrt(3)])
    b2 = np.array([2*np.pi,2*np.pi/np.sqrt(3)])
    kx = (b1[0]*r1 + b2[0]*r2)/points
    ky = (1e-4+b1[1]*r1 + b2[1]*r2)/points
    mesh = np.swapaxes(np.array([kx.flatten(),ky.flatten(), np.zeros_like(kx.flatten())]),0,1)
    return mesh


def calculate_chern_nos(params, ff_file, ff_vec=None, dir_path=None, points=60, nband=4, ham_func = None):


    # def u(esys,r,s,n,d):
    #     bra = esys[r%points,s%points,:,n+1]
    #     if d == 1:
    #         ket = esys[(r+1)%points,s%points,:,n+1]
    #     if d == 2:
    #         ket = esys[r%points,(s+1)%points,:,n+1]
    #     result = np.vdot(bra,ket)
    #     result = result/np.absolute(result)
    #     return result


    def hamiltonian(mesh, ff_vec):
        # eigensys = np.zeros((points**2,nband,nband+1),dtype=complex)
        if ham_func is None:
            # hamiltonians = pham.ham(mesh, ff_vec, params, ff_file)
            hamiltonians = np.zeros_like(mesh)
        else:
            hamiltonians = ham_func(mesh, ff_vec, ff_file)
        # if nband == 2:
        #     hamiltonians = hamiltonians[:,1:3,1:3]
        try:
            energies, eigensys = np.linalg.eigh(hamiltonians)
        except np.linalg.LinAlgError:
            print(params)
            raise SystemExit
        return energies, eigensys


    mesh = setup_mesh(points)

    if ff_vec is None:
        ff_vec = ff_load.get_ff_vec(dir_path, params['fil'], params['theta'])

    energies, eigensys = hamiltonian(mesh, ff_vec)
    eigensys = np.reshape(eigensys,(points,points,nband,nband))
    # chern = np.zeros(nband,dtype=complex)
    # F = np.zeros((points,points,nband),dtype=complex)

    eigensys = np.swapaxes(eigensys,2,3)
    conj_eigensys = np.conjugate(eigensys)

    u1 = np.einsum(
        'rsni,rsni->rsn',
        np.roll(conj_eigensys,1,axis=0),
        eigensys
    )
    u1 = u1/np.abs(u1)
    u2 = np.einsum(
        'rsni,rsni->rsn',
        np.roll(conj_eigensys,(1,1),axis=(0,1)),
        np.roll(eigensys,1,axis=0)
    )
    u2 = u2/np.abs(u2)
    u3 = np.einsum(
        'rsni,rsni->rsn',
        np.roll(conj_eigensys,1,axis=1),
        np.roll(eigensys,(1,1),axis=(0,1))
    )
    u3 = u3/np.abs(u3)
    u4 = np.einsum(
        'rsni,rsni->rsn',
        conj_eigensys,
        np.roll(eigensys,1,axis=1)
    )
    u4 = u4/np.abs(u4)
    F = -np.log( u1 * u2 * u3 * u4 )
    chern = np.sum(np.imag(F),axis=(0,1))/(2*np.pi)
    chernnos = np.round(np.real(chern))
    print(chernnos)

    # for n in [0,1]:
    #     for r in range(0,points):
    #         for s in range(0, points):
    #             F[r,s,n] = -np.log(
    #                 u(eigensys,r,s,n,1)*u(eigensys,r+1,s,n,2)*np.conjugate(u(eigensys,r,s+1,n,1))*np.conjugate(u(eigensys,r,s,n,2))
    #                 )
    #             while np.imag(F[r,s,n])>np.pi:
    #                 print('too high')
    #                 F[r,s,n]=F[r,s,n]-2*np.pi*1j
    #             while np.imag(F[r,s,n])<=-np.pi:
    #                 print('too low')
    #                 F[r,s,n]=F[r,s,n]+2*np.pi*1j
    #     chern[n] = np.sum(np.imag(F[:,:,n]))/(2*np.pi)

    # chernnos = np.round(np.real(chern))

    return chernnos, energies, eigensys, F


def main():
    #kinetic hopping parameters

    # mu = 2
    # mu = 1.448059 #alpha 0, fil 0.4
    # mu = 1.510344 #alpha 0.3, fil 0.4
    # mu = 2.1265 #alpha 0, fil 0.2
    mu = 2.13712 #alpha 0.1, fil 0.2

    params = {
        'fil' :0.2,
        't'   :1,
        'alph':0.1,
        'mu'  :mu,
        'theta': 0
    }

    # name = f"images/chern/n_{params['fil']:.3f}_alpha_{params['alph']:.3f}"

    dir_path = "/Users/mbunney/OneDrive/Documents/PhD_Research/triangular_frg/" +\
                    f"data/n_{params['fil']:.3f}/alpha_{params['alph']:.3f}/"

    nband = 4
    points = 60

    ff_file = h5py.File(dir_path + 'ff.h5', 'r')

    theta_vals = np.linspace(0,np.pi/4, num=11, endpoint=True)
    mesh = setup_mesh(points)
    kx = np.reshape(mesh[:,0],(points,points))
    ky = np.reshape(mesh[:,1],(points,points))
    for _, th in enumerate(theta_vals):
        params['theta'] = th
        chernnos, eigensys, F = calculate_chern_nos(params,
                                                    ff_file, dir_path=dir_path,
                                                    points=points, nband=nband)

        fig = plot_2D(kx,ky,np.real(eigensys[:,:,:2,0]),'E', vmax=0)
        name = f"images/chern/theta/mu_{params['mu']:.3f}_alpha_{params['alph']:.3f}_{th:.3f}"
        fig.savefig(f"{name}_energies.png", bbox_inches='tight', dpi =500)

        fig1 = plot_2D(kx,ky,np.imag(F[:,:,:2]),'F', ax_lab = chernnos[:2], cmap='coolwarm')
        fig1.savefig(f"{name}_berry_flux.png", bbox_inches = 'tight', dpi = 500)

        fig2 = plot_high_sym(np.imag(F[np.arange(points),np.arange(points),:2]),points)
        fig2.savefig(f"{name}_flux_high_symm.png", bbox_inches = 'tight', dpi = 500)


if __name__ == "__main__":
    main()
