import sys
import os
import numpy as np
import h5py
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.colors import LinearSegmentedColormap

sys.path.append(os.path.join(os.path.dirname(__file__), '..'))
import header
import triangular
import colors

nband = 2
latex_col = 7.05687

ts = [0, 1, 0, 0, 0., 0.00, 0, 0]
alphas = [0, 0]

mpl.rc('font', **{'family': 'serif',
                         'serif': ['Computer Modern Roman'], 'size': 8})
mpl.rc('text', usetex=True)
mpl.rc('text.latex', preamble=r'\usepackage{amsmath}'
              + r'\usepackage{braket}'
              + r'\usepackage{amssymb}'
              + r'\newcommand{\bvec}[1]{\boldsymbol{#1}}')


def draw_label(ax, x, y, label):
    ax.text(x, y, label, color='k', ha='center', va='center', fontsize=7.7,
            bbox=dict(boxstyle="circle,pad=0.1", fc='#ffffff88',
                      ec='k', lw=0.5))


def plot_hexagon(ax, b1, b2):
    G = np.array([0., 0.])

    M = [np.zeros(2)] * 6
    K = [np.zeros(2)] * 6

    norm = np.linalg.norm(b1 / 2)

    M[0] = b1 / 2
    M[1] = np.array([0, +norm])
    M[2] = b2 / 2
    M[3] = -b1 / 2
    M[4] = -b2 / 2
    M[5] = np.array([0, -norm])

    K[0] = - b1 / 3 + 1 * b2 / 3
    K[1] = + b1 / 3 + 2 * b2 / 3
    K[2] = + b2 / 3 + 2 * b1 / 3
    K[3] = -K[0]
    K[4] = -K[1]
    K[5] = -K[2]

    ax.plot([K[0][0], K[1][0], K[2][0], K[3][0], K[4][0], K[5][0], K[0][0]],
            [K[0][1], K[1][1], K[2][1], K[3][1], K[4][1], K[5][1], K[0][1]],
            color=colors.gray, linewidth=0.5)


def plot_FS(ax, x, y, E, color):
    nband = E.shape[-1]

    for ii in range(nband):
        ax.contour(x, y, E[:, :, ii], levels=[0], c=color,
                      linestyles='--')

    ax.set_aspect('equal')
    ax.set_xticks([-np.pi, 0, np.pi])
    ax.set_yticks([-np.pi, 0, np.pi])
    ax.set_xticklabels([r'$-\pi$', '0', r'$\pi$'])
    ax.set_yticklabels([r'$-\pi$', '0', r'$\pi$'])
    ax.set_xlabel(r'$k_x$')
    ax.set_ylabel(r'$k_y$')


def diagonalize(k, f, t, alpha, mu=0.):
    e = np.zeros((len(k), nband), dtype=complex)
    for ii in range(len(k)):
        e[ii, :] = np.linalg.eigvalsh(f(k[ii], t, alpha))
    return np.real(e - mu)


def calc_bands(ks, ts, alphas, mu):
    E = diagonalize(ks, triangular.h0, ts, alphas, mu)
    E = E.reshape(len(ks), nband)
    return E


def get_bz_pts(basis, n_k=100):
    kmesh = header.make_mesh(n_k, basis)
    border_points = []

    def backfold(basis, Qs, bp=border_points):
        def backfold_to_first_BZ(basis, Q, bp=bp):
            Q_out = Q
            overflow = []
            d = np.linalg.norm(Q)
            for nn in [-1, 0, 1]:
                for mm in [-1, 0, 1]:
                    Q_offset = Q + mm * basis[0] + nn * basis[1]
                    d_new = np.linalg.norm(Q_offset)

                    if np.isclose(d_new, d):
                        overflow.append(Q_offset)
                    elif d_new < d:
                        d = d_new
                        Q_out = Q_offset
                        overflow = []

            bp += overflow
            return Q_out

        out = np.array([backfold_to_first_BZ(basis, qq) for qq in Qs])
        return out

    bz_pts = backfold(basis, kmesh)
    boundary = np.array(border_points)

    bz_pts = np.concatenate((bz_pts, boundary), axis=0)

    return bz_pts


def contour_fs(bz_pts, n, alpha, ax):
    mu = 0

    log_path = './logs/n_{:.3f}'.format(n)\
        + '/alpha_{:.3f}'.format(alpha) + '/out.log'
    try:
        log_file = open(log_path, 'r')
        lines = log_file.readlines()
        for line in lines:
            if line.startswith("[INFO]: mu ="):
                mu = float(line.rstrip().split(' = ')[1])
    except FileNotFoundError:
        print("Did not find: " + log_path)
        return
    E = calc_bands(bz_pts, ts, [alpha, 0.0], 0.0)  # mu included as contour
    ax.set_aspect('equal')

    for ii in range(nband):
        ax.tricontour(bz_pts[:, 0], bz_pts[:, 1], E[:, ii],
                      levels=[mu], colors=[colors.first], linewidths=1.0,
                      linestyles='solid')
    ax.axis('off')


def non_cont_positions(bz_pts, basis):
    size = 0.58 * 7.05687

    fills = np.array([0.325, 0.375, 0.2, 0.175, 0.15, 0.25])
    alphas = np.array([0.0, 0.5, 0.65, 0.7, 0.8, 1.1])

    fig, axs = plt.subplot_mosaic([['A', 'B', 'C'], ['D', 'E', 'F']],
                                  figsize=(size, 0.7 * size))

    order = ['A', 'B', 'C', 'D', 'E', 'F']

    for ii in range(len(alphas)):
        ax = axs[order[ii]]

        contour_fs(bz_pts, fills[ii], alphas[ii], ax)
        plot_hexagon(ax, basis[0], basis[1])
        draw_label(ax, -3.9, 3, str(ii + 1))

    plt.subplots_adjust(wspace=0.1, hspace=0.1)
    plt.savefig('../fs_plot_cont.pdf', bbox_inches='tight')


def fix_border(mesh_bf, evals_bf, basis):
    def backfold_to_first_BZ(basis, Q, bp=bp):
        Q_out = Q
        overflow = []
        d = np.linalg.norm(Q)
        for nn in [-1, 0, 1]:
            for mm in [-1, 0, 1]:
                Q_offset = Q + mm * basis[0] + nn * basis[1]
                d_new = np.linalg.norm(Q_offset)

                if np.isclose(d_new, d):
                    overflow.append(Q_offset)

        bp += overflow
        return Q_out

    full_mesh = mesh_bf

    for ii, qq in enumerate(mesh_bf):
        border = np.array([0, 0])
        backfold_to_first_BZ(basis, Q, border)

        if not np.isclose(boder, np.array([0, 0])):
            for jj in len(border):
                evals_bf = np.stack(evals_bf, evals[ii], axis=0)

            full_mesh = np.stack(full_mesh, border, axis=0)
    return full_mesh, evals


def Q_plot(axs):
    evals = np.load('../triangular_Q/evals_bf.npy')
    mesh_bf = np.load('../triangular_Q/mesh_bf.npy')

    alphas = np.load('../triangular_Q/alphas_bf.npy')
    fills = np.load('../triangular_Q/fills_bf.npy')

    for ii in range(len(mesh_bf)):
        kpt = -1. * mesh_bf[ii]
        e = evals[:, ii]

        if not np.isclose(np.min(np.abs(np.sum(mesh_bf - kpt, axis=1))), 0):
            mesh_bf = np.vstack((mesh_bf, kpt))
            evals = np.append(evals, e[:, None], axis=1)

    color_list = [np.array([1, 1, 1]), colors.onehalf * 1.4,
                  np.array([0, 0, 0])]
    cmap = LinearSegmentedColormap.from_list('custom', color_list, N=100)

    for ii in range(len(axs)):
        ax = axs[ii]
        ax.tricontourf(mesh_bf[:, 0], mesh_bf[:, 1], np.log10(-evals[ii, :]),
                       20, cmap=cmap)
        ax.set_aspect('equal')
        ax.axis('off')


def sdw_positions(bz_pts, basis):
    size = 0.38 * 7.05687

    alphas = [0.0, 0.0, 0.6, 0.65, 0.85, 1.05]
    fills = [0.275, 0.325, 0.25, 0.275, 0.25, 0.175]

    fig, axs = plt.subplot_mosaic([['A', 'B', 'C'],
                                   ['D', 'E', 'F'],
                                   ['G', 'H', 'I'],
                                   ['J', 'L', 'M']],
                                  figsize=(size, 1.3 * size))

    order = ['A', 'D', 'B', 'E', 'C', 'F']

    for ii in range(len(alphas)):
        ax = axs[order[ii]]

        contour_fs(bz_pts, fills[ii], alphas[ii], ax)
        plot_hexagon(ax, basis[0], basis[1])
        draw_label(ax, -3.9, 3, str(ii + 1))

    Q_plot([axs['G'], axs['J'], axs['H'], axs['L'], axs['I'], axs['M']])
    for ii, nn in enumerate(['G', 'J', 'H', 'L', 'I', 'M']):
        ax = axs[nn]
        plot_hexagon(ax, basis[0], basis[1])
        draw_label(ax, -3.9, 3, str(ii + 1))

    plt.subplots_adjust(wspace=0.05, hspace=0.05)
    plt.savefig('fs_plot_sdw.pdf', bbox_inches='tight')


if __name__ == "__main__":
    file = ('mesh.h5')
    basis = header.get_basis(h5py.File(file))

    bz_pts = get_bz_pts(basis, 100)

    # sdw_positions(bz_pts, basis)
    non_cont_positions(bz_pts, basis)
