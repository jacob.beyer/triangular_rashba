import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as mpc


def maximum_distance(P, start, c1, c2):
    def line(p1, p2):
        A = (p1[1] - p2[1])
        B = (p2[0] - p1[0])
        C = (p1[0] * p2[1] - p2[0] * p1[1])
        return A, B, -C

    def intersection(L1, L2):
        D = L1[0] * L2[1] - L1[1] * L2[0]
        Dx = L1[2] * L2[1] - L1[1] * L2[2]
        Dy = L1[0] * L2[2] - L1[2] * L2[0]
        if D != 0:
            x = Dx / D
            y = Dy / D
            return x, y
        else:
            return False
    return np.linalg.norm(intersection(line(start, P), line(c1, c2)) - start)


def relative_dist_in_triangle(P, A, B, C):

    dA = np.linalg.norm(P - A) / maximum_distance(P, A, B, C)
    dB = np.linalg.norm(P - B) / maximum_distance(P, B, A, C)
    dC = np.linalg.norm(P - C) / maximum_distance(P, C, A, B)

    if np.allclose(P, A):
        dA = 0
    if np.allclose(P, B):
        dB = 0
    if np.allclose(P, C):
        dC = 0

    return dA, dB, dC


def pos_color_for_array(d1, d2, d3, c1, c2, c3):
    def pos_color(d1, d2, d3, c1, c2, c3):
        combined_color = (1 - d1) * c1 + (1 - d2) * c2 + (1 - d3) * c3
        combined_color /= np.linalg.norm(combined_color)
        combined_color = np.append(combined_color, 1)
        return combined_color

    S = d1.shape
    result = np.zeros((d1.size, 4))

    for i in range(d1.size):
        result[i, :] = pos_color(d1.flatten()[i], d2.flatten()[i],
                                 d3.flatten()[i], c1, c2, c3)

    return result.reshape((*S, 4))


if __name__ == "__main__":
    import sys
    import os
    sys.path.append(os.path.join(os.path.dirname(__file__), '..'))
    import colors

    P = np.random.rand(2)
    A = np.random.rand(2) + np.array([0.5, 0])
    B = np.random.rand(2) + np.array([0, 0.5])
    C = np.random.rand(2) - np.array([0.5, 0.5])

    P = (np.random.rand(10000, 2)) * 2 - 0.5

    ds = np.array([relative_dist_in_triangle(q, A, B, C) for q in P])

    col = pos_color_for_array(ds[:, 0], ds[:, 1], ds[:, 2], colors.third,
                              colors.second, colors.first)
    col[col < 0] = 0
    col[col > 1] = 1

    pts = np.array([A, B, C, A])
    print(pts)
    plt.scatter(P[:, 0], P[:, 1], c=col)
    plt.plot(pts[:, 0], pts[:, 1], c='k')
    plt.show()
