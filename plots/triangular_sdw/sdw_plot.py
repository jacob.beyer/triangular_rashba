import h5py
import sys
import os
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.colors as mpc
from mpl_toolkits.axes_grid1.inset_locator import inset_axes

import q_cmap as dwc

sys.path.append(os.path.join(os.path.dirname(__file__), '..'))
import header
import colors
import triangular

matplotlib.rc('font', **{'family': 'serif',
                         'serif': ['Computer Modern Roman'], 'size': 8})
matplotlib.rc('text', usetex=True)
matplotlib.rc('text.latex', preamble=r'\usepackage{amsmath}'
              + r'\usepackage{braket}'
              + r'\usepackage{amssymb}'
              + r'\newcommand{\bvec}[1]{\boldsymbol{#1}}')

color_one = colors.gray
color_two = colors.third
color_three = colors.second
color_vH = 'k'

ns = np.arange(0.15, 0.4, 0.025)
alphas = np.arange(0, 1.21, 0.05)


def draw_label(ax, x, y, label):
    ax.text(x, y, label, color='k', ha='center', va='center', fontsize=7.7,
            bbox=dict(boxstyle="circle, pad=0.1", fc='#ffffff00',
                      ec='k', lw=0.5))


def savitzky_golay(y, window_size, order, deriv=0, rate=1):
    from math import factorial

    if window_size % 2 != 1 or window_size < 1:
        raise TypeError("window_size size must be a positive odd number")
    if window_size < order + 2:
        raise TypeError("window_size is too small for the polynomials order")
    order_range = range(order + 1)
    half_window = (window_size - 1) // 2
    # precompute coefficients
    b = np.mat([[k**i for i in order_range]
                for k in range(-half_window, half_window + 1)])
    m = np.linalg.pinv(b).A[deriv] * rate**deriv * factorial(deriv)
    # pad the signal at the extremes with
    # values taken from the signal itself
    firstvals = y[0] - np.abs(y[1:half_window + 1][::-1] - y[0])
    lastvals = y[-1] + np.abs(y[-half_window - 1:-1][::-1] - y[-1])
    y = np.concatenate((firstvals, y, lastvals))
    return np.convolve(m[::-1], y, mode='valid')


upper_vH = np.array([
    [0.24921465968586393, 0.42439024390243907],
    [0.25130890052356025, 0.4185365853658537],
    [0.27434554973821984, 0.35414634146341467],
    [0.29424083769633513, 0.3092682926829269],
    [0.3172774869109947, 0.2751219512195122],
    [0.3413612565445026, 0.24878048780487805],
    [0.3633507853403141, 0.22634146341463418],
    [0.3832460732984293, 0.2078048780487805],
    [0.4020942408376963, 0.19219512195121957],
    [0.418848167539267, 0.17853658536585365],
    [0.43246073298429333, 0.16487804878048784],
    [0.4450261780104713, 0.15414634146341472],
    [0.45549738219895286, 0.1453658536585366]])

lower_vH = np.array([
    [0.24921465968586393, 0.42536585365853663],
    [0.24502617801047116, 0.4204878048780488],
    [0.2418848167539267, 0.4],
    [0.23350785340314134, 0.38634146341463416],
    [0.22722513089005242, 0.3746341463414634],
    [0.22094240837696338, 0.3619512195121951],
    [0.2178010471204188, 0.3502439024390244],
    [0.21465968586387435, 0.33951219512195124],
    [0.20942408376963356, 0.33073170731707324],
    [0.2010471204188481, 0.3336585365853659],
    [0.1968586387434555, 0.3678048780487805],
    [0.18219895287958116, 0.40390243902439027],
    [0.16335078534031416, 0.3590243902439024]])


def get_image_extent():
    alpha = (np.min(alphas), np.max(alphas),
             np.max(np.abs(np.diff(alphas))))
    nu = (np.min(ns), np.max(ns),
             np.max(np.abs(np.diff(ns))))
    extent = (nu[0] - nu[2] * 0.5, nu[1] + nu[2] * 0.5,
               alpha[0] - alpha[2] * 0.5, alpha[1] + alpha[2] * 0.5)
    return extent


def load():
    # Load the required data (hopefully all of it)
    ncut = len(ns)

    phase = np.load("phase.npy")[:ncut, :]
    Q = np.load("Q.npy")[:ncut, :]
    # Cut the last dimension if it exists
    Q = Q[:, :, :2]

    return phase, Q


def sdw_plot(phase, Q):
    fig, ax = plt.subplots(1, 1, figsize=(0.23 * 7.05687, 2 * 0.23 * 7.05687))
    x, y = np.meshgrid(ns, alphas, indexing='ij')

    aspect = (get_image_extent()[1] - get_image_extent()[0]) /\
                (get_image_extent()[3] - get_image_extent()[1])

    # DW instabilities
    DW_mask = np.zeros(phase.shape)
    DW_mask[np.where(phase == 1)] += 1

    def get_hsp():
        def find_correct_pt(pts):
            center_point = np.array([0.5, -2.2])
            return pts[np.argmin(np.sum(np.abs(pts - center_point), axis=1))]
        Gs, Ks, Ms = triangular.get_hsp()

        G = find_correct_pt(Gs)
        K = find_correct_pt(Ks)
        M = find_correct_pt(Ms)

        return G, K, M

    def backfold(basis, Qs):
        def backfold_to_first_BZ(basis, Q):
            Q_out = Q
            d = np.linalg.norm(Q)
            for nn in [-1, 0, 1]:
                for mm in [-1, 0, 1]:
                    Q_offset = Q + mm * basis[0] + nn * basis[1]
                    d_new = np.linalg.norm(Q_offset)
                    if d_new < d or np.isclose(d_new, d):
                        d = d_new
                        Q_out = Q_offset
            return Q_out

        out = np.array([backfold_to_first_BZ(basis, qq) for qq in Qs])
        return out

    def wedge_reduce(Qs):
        def wedge_reduce_point(Q):
            mat = np.array([[np.cos(np.pi / 3), -np.sin(np.pi / 3)],
                            [np.sin(np.pi / 3), np.cos(np.pi / 3)]])

            center = np.array([0.5, -2.2])

            Q_out = Q
            d = np.linalg.norm(Q - center)

            rotator = np.array([[1, 0], [0, 1]])

            for ii in range(6):
                rotator = np.einsum('ij, jk -> ik', rotator, mat)

                Q_offset = np.einsum('ij, j -> i', rotator, Q)

                for jj in [-1, 1]:
                    mirror = np.array([[jj, 0], [0, 1]])

                    Q_offset = np.einsum('ij, j -> i', mirror, Q_offset)

                    d_new = np.linalg.norm(Q_offset - center)
                    if d_new < d:
                        d = d_new
                        Q_out = Q_offset
            return Q_out

        out = np.array([wedge_reduce_point(qq) for qq in Qs])
        return out

    def distance_to_hsp(qs):
        S = qs.shape
        q_lin = qs.reshape((-1, 2))

        ds = np.zeros((q_lin.shape[0], 3))

        G, K, M = get_hsp()

        for ii, qq in enumerate(q_lin):
            ds[ii] = dwc.relative_dist_in_triangle(qq, G, K, M)

        return ds.reshape((*S[:-1], 3))

    basis = header.get_basis(h5py.File('mesh.h5'))
    Q = Q.reshape((-1, 2))
    Q = backfold(basis, Q)
    Q = wedge_reduce(Q)
    Q = Q.reshape((len(ns), len(alphas), 2))

    # triangular.plot_hexagon(plt.gca(), basis[0], basis[1])
    # plt.scatter(Q[:, :, 0], Q[:, :, 1])
    # plt.show()

    distances = distance_to_hsp(Q)

    DWC = dwc.pos_color_for_array(distances[:, :, 0], distances[:, :, 1],
                                  distances[:, :, 2],
                                  color_one, color_two, color_three)

    DWC_mask = np.dstack([DW_mask] * 4)

    for ii in range(DWC.shape[0]):
        for jj in range(DWC.shape[1]):
            if not DW_mask[ii, jj]:
                DWC[ii, jj, 0] = 1.
                DWC[ii, jj, 1] = 1.
                DWC[ii, jj, 2] = 1.
                DWC[ii, jj, 3] = 1.

    DWC = np.einsum('abc->bac', DWC)
    DWC_mask = np.einsum('abc->bac', DWC_mask)

    DW_cbar = ax.imshow(DWC, aspect='auto', extent=get_image_extent(),
                             origin='lower', interpolation='none')

    # Set the axis properties of the normal plot
    fig.subplots_adjust(left=0.11, right=0.89, bottom=0.12, top=0.89,
                              wspace=0.4, hspace=0)

    ax.set_xlabel(r'$\nu$')
    ax.set_ylabel(r'$\alpha$')
    ax.set_xlim([np.min(x) - 0.05, np.max(x) * 1.1])
    ax.set_ylim([np.min(y) - 0.02, np.max(y) + 0.05])

    # Plot the van Hove singularity line
    yhat = savitzky_golay(upper_vH[:, 0], 7, 3)
    ax.plot(yhat, np.arange(0, 1.21, 0.1), linestyle='--', c=color_vH)

    yhat = savitzky_golay(lower_vH[:, 0], 7, 3)
    ax.plot(yhat, np.arange(0, 1.21, 0.1), linestyle='--', c=color_vH)

    cax = inset_axes(ax,
                     width="80%",  # width: 5% of parent_bbox width
                     height="30%",  # height: 50%
                     loc="lower left",
                     bbox_to_anchor=(0.35, 0.75, 1, 1),
                     bbox_transform=ax.transAxes,
                     borderpad=0)

    def make_mesh(n, mesh_basis):
        k_spacings = np.linspace(0, 1, n)
        mesh_grid = np.array([np.tile(k_spacings, n),
                              np.repeat(k_spacings, n)]).T

        lin_kmesh = np.einsum("ix, xl -> il", mesh_grid, mesh_basis)
        return lin_kmesh

    G, K, M = get_hsp()

    mesh_basis = np.zeros((2, 2))
    mesh_basis[0, :] = M - G
    mesh_basis[1, :] = K - M

    lin_kmesh = make_mesh(100, mesh_basis)

    c_distances = distance_to_hsp(lin_kmesh)
    c_color = dwc.pos_color_for_array(c_distances[:, 0], c_distances[:, 1],
                                      c_distances[:, 2],
                                      color_one, color_two, color_three)

    # Fix any coloring issues we might have incurred
    for ii in range(c_color.shape[0]):
        if np.min(c_color[ii]) < 0 or np.max(c_color[ii]) > 1:
            c_color[ii, :] = 0
        if np.isnan(c_color[ii]).any():
            c_color[ii, :] = 0

    cut_angle = np.arctan(K[1] / K[0])
    for ii in range(c_color.shape[0]):
        if np.arctan(lin_kmesh[ii, 1] / lin_kmesh[ii, 0]) > cut_angle:
            c_color[ii, :] = 0

    bdy = np.array([0.1 * K[::-1], K[::-1], M[::-1], G, 0.5 * K[::-1]])
    cax.plot(bdy[:, 0], bdy[:, 1], c='k')

    cax.scatter(lin_kmesh[:, 1], lin_kmesh[:, 0], c=c_color, s=0.2)
    cax.invert_xaxis()
    cax.invert_yaxis()
    fig.text(0.37, 0.97, r'$\Gamma$')
    fig.text(0.90, 0.97, r'$M$')
    fig.text(0.90, 0.73, r'$K$')

    cax.patch.set_alpha(0.0)
    cax.axis('off')
    # Save the figure
    fig.subplots_adjust(bottom=0.1, top=0.95, left=0.2, right=0.8)

    mark_fill = np.load('../triangular_Q/fills_bf.npy')
    mark_alpha = np.load('../triangular_Q/alphas_bf.npy')

    for ii in range(len(mark_fill)):
        draw_label(ax, mark_fill[ii], mark_alpha[ii], str(ii + 1))

    plt.savefig('analysis.pdf')


sdw_plot(*load())
