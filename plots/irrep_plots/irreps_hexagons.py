import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rc
import sys
import os
import h5py
from matplotlib.patches import RegularPolygon


sys.path.append(os.path.join(os.path.dirname(__file__), '..'))
import colors
import triangular
import header

dir_path = './'

rc('font', **{'family': 'serif', 'serif': ['Computer Modern Roman'],
              'size': 8})
rc('text', usetex=True)
rc('text.latex', preamble=r'\usepackage{amsmath}')

latex_wide = 6.4567



w= 0.3*0.5*latex_wide


label_sets = [
    [1,1,1,1,1,1],
    [1,-1,1,-1,1,-1],
    [2,1,-1,-2,-1,1],
    [0,1,1,0,-1,-1],
    [2,-1,-1,2,-1,-1],
    [0,1,-1,0,-1,1],
]
file_name = ['s','f','px','py','dx2y2','dxy']
name = ['s', 'f', 'p_x', 'p_y', r'd_{x^2-y^2}', r'd_{xy}' ]
has = ['left', 'center','center', 'right', 'center', 'center']
vas = ['center', 'bottom', 'bottom', 'center', 'top', 'top']

va = 2*np.pi/6 * np.arange(6)
dist = 1.2

def plot_hex(labels, name, filen):
    fig, ax = plt.subplots(figsize=(w,w))
    bz = RegularPolygon((0, 0),
                    numVertices=6,
                    radius= 1,
                    orientation= np.pi / 6,
                    alpha=1,
                    edgecolor='k', linewidth=1,
                    fill=False)
    ax.add_patch(bz)
    for i, l in enumerate(labels):
        ax.text(dist*np.cos(va[i]), dist*np.sin(va[i]), l, 
                fontsize=10, ha=has[i], va=vas[i]
                )
    
    ax.text(0,0,r'$\Delta_{' + name + r'}$',fontsize=10,
            ha='center', va='center')

    edge=0.2
    ax.set_xlim([-dist-edge,dist+edge])
    ax.set_ylim([-dist-edge,dist+edge])
    ax.set_aspect(1)
    ax.set_axis_off()
    fig.savefig('./plots/irrep_plots/' + filen + '.pdf')
    # plt.close()


for i, labels in enumerate(label_sets):
    plot_hex(labels, name[i], file_name[i])

# plt.show()