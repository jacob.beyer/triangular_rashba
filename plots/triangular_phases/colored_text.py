import matplotlib.pyplot as plt
from matplotlib import transforms

def get_full_size( ax, x, y, list_of_strings, **kw ):
    fig = ax.get_figure()
    try:
        t = kw['transform']
        kw.pop('transform')
    except KeyError:
        t = ax.transData
    string = ''
    for s in list_of_strings:
        string += s
    tx = plt.text( x, y, string, transform=t, **kw )
    bb = tx.get_window_extent(renderer=fig.canvas.get_renderer())
    tx.remove()
    return bb.transformed(fig.transFigure.inverted())

def text(ax, x, y, list_of_strings, list_of_colors, **kw):
    ls = list_of_strings
    lc = list_of_colors
    bb = get_full_size( ax, x, y, list_of_strings, **kw )
    try:
        kw.pop('transform')
    except KeyError:
        pass
    fig = ax.get_figure()
    t = fig.transFigure
    x, y = bb.xmin, bb.ymin

    #fig.text( x, y, 'o', c='blue', ha='left', va='bottom' )

    kw['ha'] = 'left'
    kw['va'] = 'bottom'
    for s,c in zip(ls,lc):
        text = plt.text(x,y,s,color=c, transform=t, **kw)
        text.draw(fig.canvas.get_renderer())
        ex = text.get_window_extent()
        t = transforms.offset_copy(text._transform, x=ex.width, units='dots')
