import h5py
import sys
import os
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.colors as mpc
import colored_text as cct

import dw_cmap as dwc

sys.path.append(os.path.join(os.path.dirname(__file__), '..'))
import colors
import header
import triangular

matplotlib.rc('font', **{'family': 'serif',
                         'serif': ['Computer Modern Roman'], 'size': 8})
matplotlib.rc('text', usetex=True)
matplotlib.rc('text.latex', preamble=r'\usepackage{amsmath}'
              + r'\usepackage{braket}'
              + r'\usepackage{amssymb}'
              + r'\newcommand{\bvec}[1]{\boldsymbol{#1}}')

blue_color = colors.first
reddish_color = colors.third
purplish_color = 'black'

ns = np.arange(0.15, 0.61, 0.025)
alphas = np.arange(0, 1.21, 0.05)


def draw_label(ax, x, y, label):
    ax.text(x, y, label, color='k', ha='center', va='center', fontsize=7.7,
            bbox=dict(boxstyle="circle, pad=0.1", fc='#ffffff00',
                      ec='k', lw=0.5))


def savitzky_golay(y, window_size, order, deriv=0, rate=1):
    from math import factorial

    if window_size % 2 != 1 or window_size < 1:
        raise TypeError("window_size size must be a positive odd number")
    if window_size < order + 2:
        raise TypeError("window_size is too small for the polynomials order")
    order_range = range(order + 1)
    half_window = (window_size - 1) // 2
    # precompute coefficients
    b = np.mat([[k**i for i in order_range]
                for k in range(-half_window, half_window + 1)])
    m = np.linalg.pinv(b).A[deriv] * rate**deriv * factorial(deriv)
    # pad the signal at the extremes with
    # values taken from the signal itself
    firstvals = y[0] - np.abs(y[1:half_window + 1][::-1] - y[0])
    lastvals = y[-1] + np.abs(y[-half_window - 1:-1][::-1] - y[-1])
    y = np.concatenate((firstvals, y, lastvals))
    return np.convolve(m[::-1], y, mode='valid')


upper_vH = np.array([
    [0.24921465968586393, 0.42439024390243907],
    [0.25130890052356025, 0.4185365853658537],
    [0.27434554973821984, 0.35414634146341467],
    [0.29424083769633513, 0.3092682926829269],
    [0.3172774869109947, 0.2751219512195122],
    [0.3413612565445026, 0.24878048780487805],
    [0.3633507853403141, 0.22634146341463418],
    [0.3832460732984293, 0.2078048780487805],
    [0.4020942408376963, 0.19219512195121957],
    [0.418848167539267, 0.17853658536585365],
    [0.43246073298429333, 0.16487804878048784],
    [0.4450261780104713, 0.15414634146341472],
    [0.45549738219895286, 0.1453658536585366]])

lower_vH = np.array([
    [0.24921465968586393, 0.42536585365853663],
    [0.24502617801047116, 0.4204878048780488],
    [0.2418848167539267, 0.4],
    [0.23350785340314134, 0.38634146341463416],
    [0.22722513089005242, 0.3746341463414634],
    [0.22094240837696338, 0.3619512195121951],
    [0.2178010471204188, 0.3502439024390244],
    [0.21465968586387435, 0.33951219512195124],
    [0.20942408376963356, 0.33073170731707324],
    [0.2010471204188481, 0.3336585365853659],
    [0.1968586387434555, 0.3678048780487805],
    [0.18219895287958116, 0.40390243902439027],
    [0.16335078534031416, 0.3590243902439024]])


def get_image_extent():
    alpha = (np.min(alphas), np.max(alphas),
             np.max(np.abs(np.diff(alphas))))
    nu = (np.min(ns), np.max(ns),
             np.max(np.abs(np.diff(ns))))
    extent = (nu[0] - nu[2] * 0.5, nu[1] + nu[2] * 0.5,
               alpha[0] - alpha[2] * 0.5, alpha[1] + alpha[2] * 0.5)
    return extent


def load():
    # Load the required data (hopefully all of it)
    phase = np.load("phase.npy")
    lam_crit = np.load("lam_crit.npy")
    Q = np.load("Q.npy")[:, :, :2]

    return phase, lam_crit, Q


def phase_plot(phase, lam_crit, Q):
    fig, ax = plt.subplot_mosaic('''AAAAAAABBC''',
                                 figsize=(0.38 * 7.05687, 1.5 * 0.38 * 7.05687))
    x, y = np.meshgrid(ns, alphas, indexing='ij')

    lognorm = mpc.LogNorm(vmin=1e-4, vmax=np.nanmax(lam_crit))

    SC_mask = np.zeros(lam_crit.shape)
    SC_mask[np.where(phase == 0)] += 1

    SCC = dwc.dw_color_for_array(np.ones(lam_crit.shape), lognorm(lam_crit),
                                    Cl=blue_color)

    SCC_mask = np.dstack([SC_mask] * 4)

    SCC = np.einsum('abc->bac', SCC)
    SCC_mask = np.einsum('abc->bac', SCC_mask)

    aspect = (get_image_extent()[1] - get_image_extent()[0]) /\
                (get_image_extent()[3] - get_image_extent()[1])

    SC_cbar = ax['A'].imshow(
        np.ma.masked_array(SCC, mask=np.logical_not(SCC_mask)),
        aspect='auto', extent=get_image_extent(), interpolation='none',
        origin='lower')

    # Make the colorbar for the pp instability
    sc_cmap = dwc.mpl_colormap_from_slice(np.ones(100), np.linspace(0, 1, 100),
                                          Cl=blue_color)

    dummy_im = ax['B'].imshow(np.ones((2, 2)), extent=(-2, -1, -2, -1),
                                   norm=lognorm, cmap=sc_cmap[0],
                                   rasterized=True)
    cb = plt.colorbar(dummy_im, cax=ax['C'])
    # Lambda label of cb axis
    cb.set_label(r'$\Lambda_\mathrm{c}$', labelpad=-6)

    # DW instabilities
    DW_mask = np.zeros(lam_crit.shape)
    DW_mask[np.where(phase == 1)] += 1

    def backfold(basis, Qs):
        def backfold_to_first_BZ(basis, Q):
            Q_out = Q
            d = np.linalg.norm(Q)
            for nn in [-1, 0, 1]:
                for mm in [-1, 0, 1]:
                    Q_offset = Q + mm * basis[0] + nn * basis[1]
                    d_new = np.linalg.norm(Q_offset)
                    if d_new < d or np.isclose(d_new, d):
                        d = d_new
                        Q_out = Q_offset
            return Q_out

        print(Qs.shape)
        out = np.array([backfold_to_first_BZ(basis, qq) for qq in Qs])
        return out

    def distance_to_K(qs, basis):
        def _distance_to_K(q):
            pi_pts = np.array([0, 0])
            return np.sqrt(((pi_pts - q[:2][None, :])**2).sum(axis=1)).min()

        S = qs.shape
        qf = qs.reshape((-1, 2))
        qf = backfold(basis, qf)
        dqf = np.array([_distance_to_K(q) for q in qf])
        return dqf.reshape(S[:-1])

    basis = header.get_basis(h5py.File('mesh.h5'))
    DistToPi_dw = distance_to_K(Q, basis)

    DistToPi_dw /= np.max(DistToPi_dw)

    DWC = dwc.dw_color_for_array(DistToPi_dw, lognorm(lam_crit),
                                    Cl=reddish_color)
    DWC_mask = np.dstack([DW_mask] * 4)

    DWC = np.einsum('abc->bac', DWC)
    DWC_mask = np.einsum('abc->bac', DWC_mask)

    DW_cbar = ax['A'].imshow(
        np.ma.masked_array(DWC, mask=np.logical_not(DWC_mask)),
        aspect='auto', extent=get_image_extent(), origin='lower',
        interpolation='none')

    # Make the colorbar for the ph instabilities
    dw_color_x, dw_color_y = np.meshgrid(np.linspace(0, 1, 20),
                                         np.linspace(0, 1, 100))
    dw_color = dwc.dw_color_for_array(dw_color_x, dw_color_y, Cl=reddish_color)
    ax['B'].imshow(dw_color[::-1], aspect='auto', rasterized=True)
    ax['B'].set_yticks([])
    ax['B'].set_xticks([])

    # Above axis labels
    ax['B'].text(0.42, 1.06, r'$\|\mathbf q\|$', transform=ax['B'].transAxes,
                 va='bottom', ha='center')
    cct.text(ax['B'], 0.42, 1.0, ['$0$', r'$\rightarrow$', r'$\|K\|$'],
                [dwc.grayscale(reddish_color), 'black', reddish_color],
                                       transform=ax['B'].transAxes,
             ha='center', va='bottom')

    # inside axis label of SDW / SC
    ax['B'].text(0.535, 0.03, r'\slshape\textbf{SDW}',
                 transform=ax['B'].transAxes,
                 va='bottom', ha='center', rotation=90,
                 color=dwc.dw_color(0.6, 0.9, Cl=reddish_color))
    ax['C'].text(0.570, 0.03, r'\slshape\textbf{SC}',
                 transform=ax['C'].transAxes,
                 va='bottom', ha='center', rotation=90, color=sc_cmap[0](1.0))

    ax['A'].set_xlabel(r'$\nu$')
    ax['A'].set_ylabel(r'$\alpha$')
    ax['A'].set_xlim([np.min(x) - 0.05, np.max(x) * 1.1])
    ax['A'].set_ylim([np.min(y) - 0.02, np.max(y) + 0.05])

    # Plot the van Hove singularity line
    yhat = savitzky_golay(upper_vH[:, 0], 7, 3)
    ax['A'].plot(yhat, alphas[::2], linestyle='--', color='gray')

    yhat = savitzky_golay(lower_vH[:, 0], 7, 3)
    ax['A'].plot(yhat, alphas[::2], linestyle='--', color='gray')

    xs = np.array([0.325, 0.375, 0.2, 0.175, 0.15, 0.25])
    ys = np.array([0.0, 0.5, 0.65, 0.7, 0.8, 1.1])
    for ii in range(len(xs)):
        draw_label(ax['A'], xs[ii], ys[ii], str(ii + 1))

    # Save the figure
    plt.savefig('../phases.pdf', bbox_inches='tight')


phase_plot(*load())
