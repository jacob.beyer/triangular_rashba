import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as mpc


def _get_grayscale(R, G, B):
    return 125 / 256


def grayscale(c):
    C = mpc.to_rgb(c)
    G = _get_grayscale(*C)
    return np.array([G, G, G])


def _interpolate(c1, c2, x):
    return c1 + (c2 - c1) * x


def dw_color(normed_distance_to_pi, normed_lambda_c, Cl=plt.cm.Oranges(0.8)):
    Or = np.array(mpc.to_rgb(Cl))[:3]
    _Gr = _get_grayscale(*Or)
    Gr = np.array([_Gr, _Gr, _Gr])
    color = np.zeros(4)
    color[:3] = _interpolate(Gr, Or, normed_distance_to_pi)
    color[3] = normed_lambda_c

    return color


def dw_color_for_array(normed_distances_to_pi, normed_lambda_c,
                       Cl=plt.cm.Oranges(0.8)):
    S = normed_distances_to_pi.shape

    dpi = normed_distances_to_pi.flatten()
    lc = normed_lambda_c.flatten()

    result = np.zeros((dpi.size, 4))

    for i in range(dpi.size):
        result[i, :] = dw_color(dpi[i], lc[i], Cl=Cl)

    return result.reshape((*S, 4))


def mpl_colormap_from_slice(normed_distances_to_pi, normed_lambda_c,
                            Cl=plt.cm.Oranges(0.8)):
    colors = dw_color_for_array(normed_distances_to_pi,
                                normed_lambda_c, Cl).reshape((-1, 4))
    cmap = mpc.LinearSegmentedColormap.from_list('dwcmap', colors)
    cmap_r = mpc.LinearSegmentedColormap.from_list('dwcmap_r', colors[::-1])
    return cmap, cmap_r
