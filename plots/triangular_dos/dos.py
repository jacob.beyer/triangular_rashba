import sys
import os
import numpy as np
import h5py
import matplotlib
import matplotlib.pyplot as plt
from matplotlib.colors import LinearSegmentedColormap
import matplotlib.patches as mpp

sys.path.append(os.path.join(os.path.dirname(__file__), '..'))
import header
import triangular
import plots
import colors

nband = 2
latex_col = 6.4567

matplotlib.rc('font', **{'family': 'serif',
                         'serif': ['Computer Modern Roman'], 'size': 8})
matplotlib.rc('text', usetex=True)
matplotlib.rc('text.latex', preamble=r'\usepackage{amsmath}'
              + r'\usepackage{braket}'
              + r'\usepackage{amssymb}'
              + r'\newcommand{\bvec}[1]{\boldsymbol{#1}}')

n_bin = 100

color_list = [colors.first, colors.third]
cmap = LinearSegmentedColormap.from_list('custom', color_list, N=n_bin)


def diagonalize(k, f, t, alpha, mu=0.):
    e = np.zeros((len(k), nband), dtype=complex)
    for ii in range(len(k)):
        e[ii, :] = np.linalg.eigvalsh(f(k[ii], t, alpha))
    return e - mu


def lorenzian(E, w, delta):
    return 1. / len(E) / np.pi * np.sum(delta / (np.subtract.outer(w, E)**2
                                                 + delta**2), axis=1)


def calculate_dos(n_k, n_energies, mesh_basis, mu, dos_plot=None,
                  fs_plot=None):
    lin_kmesh = header.make_mesh(n_k, mesh_basis)

    alphas = np.arange(0, 1.3, 0.1)
    alphas = np.vstack([alphas, np.zeros_like(alphas)]).T

    t = np.array([0, 1.0, 0, 0, 0, 0, 0, 0, 0])

    E = [None] * len(alphas)

    try:
        E = np.load('E_dos.npy')
    except FileNotFoundError:
        for ii in range(len(alphas)):
            E[ii] = diagonalize(lin_kmesh, triangular.h0, t, alphas[ii])
            E[ii] = E[ii].reshape(n_k, n_k, nband)
        np.save('E_dos', E)
    else:
        print("WARN: Loading old file")

    if fs_plot is not None:
        fig, ax = plt.subplots(1, 1)
        x = lin_kmesh[:, 0].reshape((n_k, n_k))
        y = lin_kmesh[:, 1].reshape((n_k, n_k))

        for ii in range(len(alphas)):
            plots.plot_FS(ax, x, y, E[ii],
                          colors.interpolate(ii, len(alphas)))
        plt.savefig(fs_plot)

    delta = 0.03

    omega = [None] * len(alphas)
    dos = [None] * len(alphas)
    filling = [None] * len(alphas)
    try:
        dos = np.load('dos.npy')
        filling = np.load('filling.npy')
    except FileNotFoundError:
        for ii in range(len(alphas)):
            omega[ii] = np.linspace(np.min(E[ii]), np.max(E[ii]), n_energies)
            dos[ii] = lorenzian(E[ii].flatten(), omega[ii], delta)

            filling[ii] = np.zeros_like(omega[ii])
            for jj in range(len(omega[ii])):
                filling[ii][jj] = np.sum(E[ii][:] < omega[ii][jj])\
                    / len(E[ii].flatten())
        np.save('dos', dos)
        np.save('filling', filling)
    else:
        print("WARN: Loading old dos & filling")

    print(dos.shape)
    if dos_plot is not None:
        fig, ax = plt.subplots(1, 1, figsize=(0.5 * latex_col,
                                              0.5 / 2 * latex_col))
        for ii in range(len(alphas)):
            if ii == 0:
                lbl = r'$\alpha = [{a0:.1f},{a1}]$'.format(
                    a0=alphas[0][0], a1=0)
            elif ii == len(alphas) - 1:
                lbl = r'$\alpha = [{a0:.1f},{a1}]$'.format(
                    a0=alphas[-1][0], a1=0)
            else:
                lbl = None
            ax.plot(filling[ii], dos[ii],
                    c=colors.interpolate(ii, len(alphas)),
                    linestyle="-", label=lbl)

        ax.set_xlim([0, 1])
        ax.yaxis.tick_right()
        ax.yaxis.set_label_position("right")
        ax.set_xlabel(r"$\nu$")
        ax.grid('major', axis='x')
        ax.set_ylim([-0.01, 0.5])
        ax.set_yticks([])

        P = mpp.Rectangle((0.45, 0.32), 0.5, 0.15, zorder=4,
                          ec='none', fc='white', alpha=0.6)
        ax.add_patch(P)

        ax2 = fig.add_axes([0.5, 0.80, 0.3, 0.05])
        norm = matplotlib.colors.Normalize(vmin=0, vmax=1.2)
        cb1 = matplotlib.colorbar.ColorbarBase(ax2, cmap=cmap, norm=norm,
                                               orientation='horizontal')
        cb1.set_label(r'$\alpha$', labelpad=-6)
        cb1.ax.set_xticks([0, 1.2])

        plt.savefig(dos_plot, bbox_inches="tight")
    return dos, filling


if __name__ == "__main__":
    file = 'mesh.h5'
    basis = header.get_basis(h5py.File(file))
    calculate_dos(300, 500, basis, 0.0, '../dos.pdf')
