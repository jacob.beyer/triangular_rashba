import matplotlib.pyplot as plt
import numpy as np


def plot_FS(ax, x, y, E, color):
    nband = E.shape[-1]

    for ii in range(nband):
        ax.contour(x, y, E[:, :, ii], levels=[0], c=color,
                      linestyles='--')

    ax.set_aspect('equal')
    ax.set_xticks([-np.pi, 0, np.pi])
    ax.set_yticks([-np.pi, 0, np.pi])
    ax.set_xticklabels([r'$-\pi$', '0', r'$\pi$'])
    ax.set_yticklabels([r'$-\pi$', '0', r'$\pi$'])
    ax.set_xlabel(r'$k_x$')
    ax.set_ylabel(r'$k_y$')
