import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rc
import sys
import os
import h5py

sys.path.append(os.path.join(os.path.dirname(__file__), '..'))
import colors
import triangular
import header

dir_path = './'

rc('font', **{'family': 'serif', 'serif': ['Computer Modern Roman'],
              'size': 8})
rc('text', usetex=True)
rc('text.latex', preamble=r'\usepackage{amsmath}')

latex_wide = 3.4039


tp = -0.0
alpha = 0.1


def backfold(basis, Qs):
    def backfold_to_first_BZ(basis, Q):
        Q_out = Q
        d = np.linalg.norm(Q)
        for nn in [-1, 0, 1]:
            for mm in [-1, 0, 1]:
                Q_offset = Q + mm * basis[0] + nn * basis[1]
                d_new = np.linalg.norm(Q_offset)
                if d_new < d or np.isclose(d_new, d):
                    d = d_new
                    Q_out = Q_offset
        return Q_out

    out = np.array([backfold_to_first_BZ(basis, qq) for qq in Qs])
    return out


def e(k, t=[0, 1., 0, 0, 0, 0, 0, 0], alpha=[0, 0]):
    return np.linalg.eigvalsh(triangular.h0(k, t, alpha))


def distance(k1, k2):
    return np.sqrt((k1[0] - k2[0])**2 + (k1[1] - k2[1])**2)


def closest(k, mesh):
    closest = np.pi
    closest_idx = -1

    for ii, kk in enumerate(mesh):
        if (distance(k, kk) < closest):
            closest_idx = ii
            closest = distance(k, kk)

    return closest_idx


def get_hsp(pts):
    N = 90

    path = np.array([pts[0]])
    node_idx = [0]

    for ii in range(len(pts) - 1):
        x = np.linspace(pts[ii][0], pts[ii + 1][0], N)
        y = np.linspace(pts[ii][1], pts[ii + 1][1], N)
        path_piece = np.stack((x, y), axis=-1)
        path = np.append(path, path_piece[1:], axis=0)
        node_idx.append(path.shape[0] - 1)

    dist = path - np.roll(path, 1, axis=0)
    dist = np.sqrt(np.sum(np.square(dist), axis=1))
    dist_pts = np.cumsum(dist)

    dist_nodes = dist_pts[node_idx]

    return path, dist_pts, dist_nodes


def get_dispersion(disp_pts, mesh, energy):
    disp = np.zeros((len(disp_pts), nband))

    for ii, kk in enumerate(disp_pts):
        nearest = closest(kk, mesh)
        for bb in range(nband):
            disp[ii, bb] = energy[nearest, bb]

    return disp


if __name__ == "__main__":
    mesh_file = h5py.File(dir_path + "mesh.h5")
    basis = header.get_basis(mesh_file)
    mesh = header.make_mesh(100, basis)

    mesh = backfold(basis, mesh)

    nband = 2

    G = 0.0 * basis[0]
    M = -.5 * basis[1]
    K = (basis[0] - basis[1]) / 3.

    pts = [G, K, M, G]
    labels = ['$\Gamma$', '$K$', '$M$', '$\Gamma$']

    path, dist_on_hsp, dist_nodes = get_hsp(pts)

    fig, ax = plt.subplots(1, 1, figsize=(1 * latex_wide,
                                          0.55 * 1.0 * latex_wide))
    ax.set_xlim(dist_nodes[0], dist_nodes[-1])
    ax.set_xticks(dist_nodes)
    ax.set_xticklabels(labels)

    ax.set_yticks([])

    for n in range(len(dist_nodes)):
        ax.axvline(x=dist_nodes[n], linewidth=0.5, color='k',
                   linestyle='dashed')
    ax.set_ylabel(r"$\epsilon_{\boldsymbol{k}}$", loc='top', rotation=0)

    # ax.spines['top'].set_visible(False)
    # ax.spines['right'].set_visible(False)

    energies = np.zeros((path.shape[0], nband))
    comp = np.zeros((path.shape[0], nband))
    for ii in range(len(path)):
        k = np.array([path[ii, 0], path[ii, 1]])
        energies[ii] = e(k, alpha=[0.1, 0])
        comp[ii] = e(k, alpha=[0.0, 0])

    ax.hlines(y=-2.137115, xmin=dist_on_hsp[0], xmax=dist_on_hsp[-1],
              linestyle='--', color=colors.gray)

    ax.plot(dist_on_hsp, energies[:, 0], color=colors.second)
    ax.plot(dist_on_hsp, energies[:, 1], color=colors.second)

    axin = ax.inset_axes([0.2, 0.4, 0.6, 0.6])

    for n in range(len(dist_nodes)):
        axin.axvline(x=dist_nodes[n], linewidth=0.5, color='k',
                     linestyle='dashed', zorder=-3)

    axin.plot(dist_on_hsp, energies[:, 0], color=colors.second)
    axin.plot(dist_on_hsp, energies[:, 1], color=colors.second)
    axin.hlines(y=-2.137115, xmin=dist_on_hsp[0], xmax=dist_on_hsp[-1],
                linestyle='--', color=colors.gray)

    axin.set_xlim(dist_nodes[0], dist_nodes[-1])
    axin.set_xticks(dist_nodes)
    axin.set_xticklabels(labels)
    axin.xaxis.tick_top()
    axin.tick_params(axis="x", direction="in", pad=-15)

    axin.set_ylim([-2.5, -1.5])
    axin.set_xlim([3.0, 7.0])

    axin.set_yticks([])

    bbox = dict(boxstyle="round", ec="white", fc="white", alpha=0.8)
    plt.setp(axin.get_xticklabels(), bbox=bbox)

    ax.indicate_inset_zoom(axin, edgecolor=colors.gray)

    fig.tight_layout()
    plt.savefig('../low_filling_bands.pdf')
