import numpy as np
import os
import sys
import matplotlib.pyplot as plt #type:ignore
import matplotlib as mpl #type:ignore
import matplotlib.patches as patches #type:ignore
import h5py #type:ignore

sys.path.append(os.path.join(os.path.dirname(__file__), '..'))
import colors #type:ignore

mpl.rc('font', **{'family': 'serif', 'serif': ['Computer Modern Roman'],
                         'size': 8})
mpl.rc('text', usetex=True)
mpl.rc('text.latex', preamble=r'\usepackage{amsmath}'
                + r'\usepackage{braket}' + r'\usepackage{amssymb}'
                + r'\newcommand{\bvec}[1]{\boldsymbol{#1}}')

fig_width = 3.4039
fig_height = 0.75*fig_width

fig, axs = plt.subplots(3, 1, figsize=(fig_width, fig_height), sharex=True,
                        height_ratios=[1, 1, 1.5], gridspec_kw={'hspace': 0.05})


data = h5py.File('filling_transition_data', 'r')
filling = data['filling'][:]
lamc = data['lamc'][:]
chern = data['chern'][:]
gap = data['gap'][:]
shell_ratios = data['shell_ratios'][:]

m = 3
lim_factor = 1.2

# Fix the first axis labeling
axs[0].semilogy(filling, lamc, '.', c='k', markersize=m)
axs[0].set_ylabel(r'$\Lambda_c$')
axs[0].set_ylim([5e-4,5e-3])
axs[0].tick_params(bottom=False, labelbottom=False)
axs[0].tick_params(which='minor', labelleft=False)
axs[0].set_yticks([5e-4,1e-3,5e-3], labels=[r'$5 \cdot 10^{-4}$',r'$10^{-3}$',r'$5 \cdot 10^{-3}$'])
axs[0].yaxis.set_label_coords(-0.09, 0.6)

axs[1].plot(filling[np.abs(chern) == 4], gap[np.abs(chern) == 4], '.',
            c= mpl.colormaps['tab10'].colors[0], markersize=m)
axs[1].plot(filling[np.abs(chern) == 8], gap[np.abs(chern) == 8], '.',
            c= mpl.colormaps['tab10'].colors[3], markersize=m)
axs[1].set_ylabel(r'$E/t$')
axs[1].set_ylim([0, lim_factor * np.amax(gap)])
axs[1].tick_params(bottom=False, labelbottom=False)
axs[1].text(0.41, 0.1, r'$C = -8$', c=mpl.colormaps['tab10'].colors[3])
axs[1].text(0.46, 0.1, r'$C = 4$', c= mpl.colormaps['tab10'].colors[0])
axs[1].yaxis.set_label_coords(-0.09, 0.5)

# TODO why are there 4 colors here?
cols = [colors.first, colors.third, colors.second, colors.third]
symb = ['s','x','.']
label = ['1st', '2nd', '3rd']
axs[2].scatter(filling, np.abs(shell_ratios[:, 0]), marker='s',
                c='k', s=m/2, label='1st')
axs[2].scatter(filling, np.abs(shell_ratios[:, 1]), marker='s',
                facecolors='none', edgecolors='k', s=2*m, label='2nd', linewidths=0.5)
axs[2].scatter(filling, np.abs(shell_ratios[:, 2]),
                c='k', s=m, label='3rd')
                # c=cols[ii],)
axs[2].legend(loc='center right', bbox_to_anchor=(1,0.55))
axs[2].set_ylim([-0.05, 1.05])
axs[2].set_ylabel(r'$w_n$')
axs[2].set_xlabel(r'$\nu$',loc='right', labelpad=-8)
axs[2].set_yticks([0, 0.5, 1], labels=['0', '', '1'])

axs[2].get_yaxis().majorTicks[2].label1.set_verticalalignment('top')

# for ii, ss in enumerate(['1st', '2nd', '3rd']):
#     ypos = 0.7 - 0.2 * ii
#     s = ss + r"-neighbor"
#     axs[2].text(0.455, ypos, s, c=cols[ii])

for ii in range(3):
    axs[ii].vlines([0.445], axs[ii].get_ylim()[0], axs[ii].get_ylim()[1],
                   colors=colors.gray, linestyle='--', linewidth=1)
    axs[ii].set_xlim([np.amin(filling) - 0.001, np.amax(filling) + 0.001])

    # patch = patches.Rectangle((0.0, 0.7), 0.07, 0.3, linewidth=1,
    #                           edgecolor='none', transform=axs[ii].transAxes,
    #                           facecolor='white', alpha=0.8, zorder=2)
    # axs[ii].add_patch(patch)

axs[0].text(0.01, 0.05, '(a)',
                 transform=axs[0].transAxes, c='k', zorder=3, va='bottom', ha='left')
axs[1].text(0.01, 0.05, '(b)',
                 transform=axs[1].transAxes, c='k', zorder=3, va='bottom', ha='left')
axs[2].text(0.01, 0.5, '(c)',
                 transform=axs[2].transAxes, c='k', zorder=3, va='center', ha='left')

fig.savefig('filling_transition.png', bbox_inches="tight", pad_inches=0.04, dpi=700)
