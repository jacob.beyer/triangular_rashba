import numpy as np
import matplotlib
import matplotlib as mpl
import matplotlib.pyplot as plt
import h5py
import sys
import os
import glob
import pickle
from matplotlib.colors import LinearSegmentedColormap

sys.path.append(os.path.join(os.path.dirname(__file__), '..'))
import header
import triangular
import colors

head_dir = './'

color_list = [colors.second, 'w']
cmap = LinearSegmentedColormap.from_list('custom', color_list, N=100)

latex_col = 6.4567

mpl.rc('font', **{'family': 'serif',
                         'serif': ['Computer Modern Roman'], 'size': 8})
mpl.rc('text', usetex=True)
mpl.rc('text.latex', preamble=r'\usepackage{amsmath}'
              + r'\usepackage{braket}'
              + r'\usepackage{amssymb}'
              + r'\newcommand{\bvec}[1]{\boldsymbol{#1}}')


if __name__ == "__main__":
    evals = np.load('evals_bf.npy')
    mesh_bf = np.load('mesh_bf.npy')

    alphas = np.load('alphas_bf.npy')
    fills = np.load('fills_bf.npy')

    print(mesh_bf.shape)
    print(evals.shape)

    for ii in range(len(mesh_bf)):
        kpt = mesh_bf[ii]
        e = evals[:, ii]

        if np.isclose(np.min(np.abs(np.sum(mesh_bf - kpt, axis=1))), 0):
            mesh_bf = np.vstack((mesh_bf, kpt))
            evals = np.append(evals, e[:, None], axis=1)

    fig, ax = plt.subplot_mosaic([['A', 'B', 'C', 'D'], ['E', 'F', 'G', 'D']],
                                 gridspec_kw={'width_ratios': [1, 1, 1, 0.1]},
                                 figsize=(6.4567, 0.49 * 6.4567))

    evals = np.log10(np.abs(evals))

    v_max = np.max(evals)
    v_min = np.min(evals)

    order = ['A', 'B', 'C', 'E', 'F', 'G']

    for ii in range(len(alphas)):
        ax[order[ii]].tricontourf(mesh_bf[:, 0], mesh_bf[:, 1],
                                         evals[ii, :], 20, cmap=cmap,
                                         vmin=v_min, vmax=v_max)
        ax[order[ii]].set_aspect('equal')
        ax[order[ii]].set_title(r'$\alpha=$' + str(alphas[ii])
                                + r' $n=$' + str(fills[ii]))
        ax[order[ii]].set_xticks([])
        ax[order[ii]].set_yticks([])

    norm = mpl.colors.Normalize(vmin=v_min, vmax=v_max)

    cbar = mpl.colorbar.ColorbarBase(ax['D'], cmap=cmap, norm=norm)
    cbar.set_label('Some Units')
    plt.show()
    plt.savefig('multi_Q.pdf')
