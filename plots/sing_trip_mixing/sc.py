import numpy as np #type:ignore
import matplotlib as mpl #type:ignore
import matplotlib.pyplot as plt #type:ignore
from matplotlib import patches #type:ignore
import cmasher #type:ignore

mpl.rc('font', **{'family': 'serif', 'serif': ['Computer Modern Roman'],
                         'size': 8})
mpl.rc('text', usetex=True)
mpl.rc('text.latex', preamble=r'\usepackage{amsmath}'
                + r'\usepackage{braket}' + r'\usepackage{amssymb}'
                + r'\newcommand{\bvec}[1]{\boldsymbol{#1}}')

# fig_width = 1.0 * 7.05687
fig_width = 3.4039

def savitzky_golay(y, window_size, order, deriv=0, rate=1):
    from math import factorial

    if window_size % 2 != 1 or window_size < 1:
        raise TypeError("window_size size must be a positive odd number")
    if window_size < order + 2:
        raise TypeError("window_size is too small for the polynomials order")
    order_range = range(order + 1)
    half_window = (window_size - 1) // 2
    # precompute coefficients
    b = np.mat([[k**i for i in order_range]
                for k in range(-half_window, half_window + 1)])
    m = np.linalg.pinv(b).A[deriv] * rate**deriv * factorial(deriv)
    # pad the signal at the extremes with
    # values taken from the signal itself
    firstvals = y[0] - np.abs(y[1:half_window + 1][::-1] - y[0])
    lastvals = y[-1] + np.abs(y[-half_window - 1:-1][::-1] - y[-1])
    y = np.concatenate((firstvals, y, lastvals))
    return np.convolve(m[::-1], y, mode='valid')

upper_vH = np.array([
    [0.24921465968586393, 0.42439024390243907],
    [0.25130890052356025, 0.4185365853658537],
    [0.27434554973821984, 0.35414634146341467],
    [0.29424083769633513, 0.3092682926829269],
    [0.3172774869109947, 0.2751219512195122],
    [0.3413612565445026, 0.24878048780487805],
    [0.3633507853403141, 0.22634146341463418],
    [0.3832460732984293, 0.2078048780487805],
    [0.4020942408376963, 0.19219512195121957],
    [0.418848167539267, 0.17853658536585365],
    [0.43246073298429333, 0.16487804878048784],
    [0.4450261780104713, 0.15414634146341472],
    [0.45549738219895286, 0.1453658536585366]])

lower_vH = np.array([
    [0.24921465968586393, 0.42536585365853663],
    [0.24502617801047116, 0.4204878048780488],
    [0.2418848167539267, 0.4],
    [0.23350785340314134, 0.38634146341463416],
    [0.22722513089005242, 0.3746341463414634],
    [0.22094240837696338, 0.3619512195121951],
    [0.2178010471204188, 0.3502439024390244],
    [0.21465968586387435, 0.33951219512195124],
    [0.20942408376963356, 0.33073170731707324],
    [0.2010471204188481, 0.3336585365853659],
    [0.1968586387434555, 0.3678048780487805],
    [0.18219895287958116, 0.40390243902439027],
    [0.16335078534031416, 0.3590243902439024]])


alphas = np.arange(0.0, 1.21, 0.05)
fills = np.arange(0.150, 0.601, 0.025)

dfills = fills[1] - fills[0]
dalphas = alphas[1] - alphas[0]

cmap = cmasher.guppy

phase = np.load('phase.npy')
sing = np.load("sing.npy")

cut = 11
alphas = alphas[:cut]
sing = sing[:, :cut]
phase = phase[:, :cut]
phase = np.array(phase, dtype='float')

fig, ax = plt.subplots(figsize=(fig_width, 0.6*fig_width))
plt.subplots_adjust(left=0.1,right=0.84)

image_kw = dict(extent=(fills[0] - 0.5 * dfills, fills[-1] + 0.5 * dfills,
                        alphas[0] - 0.5 * dalphas, alphas[-1] + 0.5 * dalphas))

im = ax.imshow(sing[:, ::-1].T, **image_kw,
                      cmap=cmap, vmin=0, vmax=1, aspect='auto')
ax.set_ylim([np.min(alphas) - 0.02, np.max(alphas) + 0.02])
ax.set_xlim(0.15 - dfills / 2, 0.60 + dfills / 2)
ax.set_xlabel(r'$\nu$', labelpad=-2)
# ax.xaxis.set_label_coords(1.03,-0.08)
ax.set_ylabel(r'$\alpha$', labelpad=-2)
# ax.yaxis.set_label_coords(-0.05, 0.88)

phase_ = np.copy(phase)
phase_[np.where(phase != 1)] = np.nan
ax.imshow(phase_[:, ::-1].T, vmin=0, vmax=2, cmap='gray', **image_kw,
                 alpha=0.7)

# phase_ = np.copy(phase)
# phase_[np.where(phase != -1)] = np.nan
# ax.imshow(phase_[:, ::-1].T, vmin=-2, vmax=-1, cmap='gray_r', **image_kw,
#                  alpha=0.7)

alphas_ = np.arange(0, 1.21, 0.1)
# Plot the van Hove singularity line
yhat = savitzky_golay(upper_vH[:, 0], 7, 3)
ax.plot(yhat, alphas_, linestyle='--', color='gray')

yhat = savitzky_golay(lower_vH[:, 0], 7, 3)
ax.plot(yhat, alphas_, linestyle='--', color='gray')

ax.text(0.15, np.amax(alphas), r'(a)', ha='left', va='top')

n = 0.5
alpha = 0.4
ax.scatter(np.array([n - 0.01]), np.array([alpha]),s=50,facecolor=None, edgecolors='k',linewidths=0.5)


im_ratio = 0.5
ax.set_aspect(im_ratio)
cax = ax.inset_axes([1.08,0,0.05,1])
cb = plt.colorbar(im, cax=cax)

cb.set_ticks([0, 1])
cb.set_label('singlet weight', labelpad=-8)
cax.yaxis.set_ticks_position('left')
cax.yaxis.set_label_position('left')

cax_right = cax.twinx()
cax_right.set_yticks([0, 1])
cax_right.set_yticklabels([r'$-$', r'$+$'])
cax_right.set_ylabel(r'Eigenvector', rotation=270, labelpad=-2)




fig.savefig('../sing_trip_mixing.png', pad_inches=0.01,bbox_inches="tight", dpi=700)
