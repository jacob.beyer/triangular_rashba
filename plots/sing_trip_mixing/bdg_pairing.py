
'''
    script to be run on Fritz/HPC (no plotting), creates h5 files with
    data to plot
'''
import numpy as np
import matplotlib.pyplot as plt #type: ignore
from matplotlib.patches import RegularPolygon #type:ignore
from matplotlib import rc #type:ignore

import cmasher #type:ignore

rc('font', **{'family': 'serif', 'serif': ['Computer Modern Roman'],
                         'size': 8})
rc('text', usetex=True)
rc('text.latex', preamble=r'\usepackage{amsmath}'
                + r'\usepackage{braket}' + r'\usepackage{amssymb}'
                + r'\newcommand{\bvec}[1]{\boldsymbol{#1}}')


I2 = np.array([[1,0],[0,1]], dtype=complex)
sx = np.array([[0,1],[1,0]],dtype=complex)
sy = np.array([[0,-1j],[1j,0]],dtype=complex)


def make_bz(col=None):
    give = 0.0
    r = 4 * np.pi / 3 + 10*give
    if col is None:
        bz = RegularPolygon((0, 0),
                        numVertices=6,
                        radius= r,
                        orientation= np.pi / 6,
                        alpha=1,
                        edgecolor='k', linewidth=0.6,
                        fill=False)
    else:
        bz = RegularPolygon((0, 0),
                        numVertices=6,
                        radius= r,
                        orientation= np.pi / 6,
                        alpha=1,
                        edgecolor='k', linewidth=0.6,
                        fc=col)


    return bz


def setup_ibz(points):

    b1 = np.array([0,4*np.pi/np.sqrt(3)])
    b2 = np.array([2*np.pi,2*np.pi/np.sqrt(3)])

    x = np.linspace(0, 2, num=points) - 1
    x, y = np.meshgrid(x, x)
    k = np.array([
        b1[0]*x + b2[0]*y,
        b1[1]*x + b2[1]*y,
        np.zeros_like(x)
    ])
    k = np.transpose(np.reshape(k, (3, points**2)))

    a = np.sqrt(np.sum(b1**2))/2
    give = 0.01
    mask = k[:, 1] > a + give
    k[:, 0][mask] = np.nan
    k[:, 1][mask] = np.nan
    k[:, 2][mask] = np.nan
    mask = k[:, 1] > -np.sqrt(3) * (k[:, 0] - 2 * a / np.sqrt(3)) + give
    k[:, 0][mask] = np.nan
    k[:, 1][mask] = np.nan
    k[:, 2][mask] = np.nan
    mask = k[:, 1] < np.sqrt(3) * (k[:, 0] - 2 * a / np.sqrt(3)) - give
    k[:, 0][mask] = np.nan
    k[:, 1][mask] = np.nan
    k[:, 2][mask] = np.nan
    mask = k[:, 1] < - a - give
    k[:, 0][mask] = np.nan
    k[:, 1][mask] = np.nan
    k[:, 2][mask] = np.nan
    mask = k[:, 1] < -np.sqrt(3) * (k[:, 0] + 2 * a / np.sqrt(3)) - give
    k[:, 0][mask] = np.nan
    k[:, 1][mask] = np.nan
    k[:, 2][mask] = np.nan
    mask = k[:, 1] > np.sqrt(3) * (k[:, 0] + 2 * a / np.sqrt(3)) + give
    k[:, 0][mask] = np.nan
    k[:, 1][mask] = np.nan
    k[:, 2][mask] = np.nan

    return k


def main():

    fill = 0.5
    alph_ = 0.4

    dir_path = './bdg_pairing/'
    param_path = f'_fill_{fill:.3f}_alph_{alph_:.3f}'

    kx = np.load(dir_path + 'kx.npy')
    ky = np.load(dir_path + 'ky.npy')

    fdx = np.load(dir_path + 'fdx' + param_path + '.npy')
    fdy = np.load(dir_path + 'fdy' + param_path + '.npy')

    # fig_width = 1.0 * 7.05687
    fig_width = 3.4039
    vertical_pad = 0.77
    horizontal_pad = 0.775
    fig, ax = plt.subplots(
        2,5,figsize=(fig_width/horizontal_pad,(np.sqrt(3)/4.1)*fig_width/vertical_pad),
        gridspec_kw = {'wspace':0, 'hspace':0,'width_ratios':[1,1,0.1,1,1]}
    )

    ax[0,2].set_axis_off()
    ax[1,2].set_axis_off()

    cmap = cmasher.guppy
    vmax = np.amax([np.nanmax(np.abs(fdx)),np.nanmax(np.abs(fdy))])
    levels = np.linspace(-vmax, vmax, num=13)

    labels = [r'$\Psi$', r'$d_x$',r'$d_y$',r'$d_z$']

    for i in range(2):
        for j in range(2):
            ax[i,j].set_axis_off()
            ax[i,j].set_aspect(1)
            ax[i,j].text(-3.2,-2.8,labels[2*i+j],ha='right')
            if np.nanmax(np.abs(fdx[:,:,2*i+j])) > 1e-7:
                try:
                    c = ax[i,j].contourf(kx,ky,fdx[:,:,2*i+j],levels,cmap=cmap,vmax=vmax,vmin=-vmax)
                    ax[i,j].contour(c,levels = levels, colors='k', linewidths=0.5)
                    bz = make_bz()
                    ax[i,j].add_patch(bz)
                except ValueError:
                    bz = make_bz()
                    ax[i,j].add_patch(bz)
                    continue
            else:
                bz = make_bz(col=cmap(0.5))
                ax[i,j].add_patch(bz)
                ax[i,j].autoscale_view()
                # bg = make_bz(cmap(0.5))
                # ax[i,j].add_patch(bg)
                continue


    for i in range(2):
        for j in range(2):
            ax[i,j+3].set_axis_off()
            ax[i,j+3].set_aspect(1)
            ax[i,j+3].text(-3.2,-2.8,labels[2*i+j],ha='right')
            if np.nanmax(np.abs(fdy[:,:,2*i+j])) > 1e-7:
                try:
                    c = ax[i,j+3].contourf(kx,ky,fdy[:,:,2*i+j],levels,cmap=cmap,vmax=vmax,vmin=-vmax)
                    ax[i,j+3].contour(c,levels = levels, colors='k', linewidths=0.5)
                    bz = make_bz()
                    ax[i,j+3].add_patch(bz)
                except ValueError:
                    bz = make_bz()
                    ax[i,j+3].add_patch(bz)
                    continue
            else:
                bz = make_bz(col=cmap(0.5))
                ax[i,j+3].add_patch(bz)
                ax[i,j+3].autoscale_view()
                # bg = make_bz(cmap(0.5))
                # ax[i,j+2].add_patch(bg)
                continue

    rect = plt.Rectangle(
        # (lower-left corner), width, height
        (0.11, 0.11), 0.39, 0.77, fill=False, color="grey", lw=0.5,
        zorder=1000, figure=fig, transform=fig.transFigure
    )
    fig.patches.extend([rect])
    rect1 = plt.Rectangle(
        # (lower-left corner), width, height
        (0.51, 0.11), 0.39, 0.77, fill=False, color="grey", lw=0.5,
        zorder=1000, figure=fig, transform=fig.transFigure
    )
    fig.patches.extend([rect1])

    fig.text(0.115, 0.87, r'(b)', ha='left', va='top')
    fig.text(0.515, 0.87, r'(c)', ha='left', va='top')


    fig.savefig(dir_path + f"../sample_sc_pairing.png", pad_inches=0.01,bbox_inches='tight',dpi=700)
    # plt.show()
    plt.close()



if __name__ == "__main__":
    main()
