import numpy as np
import os
import sys
import matplotlib.pyplot as plt
import matplotlib
import matplotlib.patches as patches
from matplotlib import ticker
import numpy.ma as ma

sys.path.append(os.path.join(os.path.dirname(__file__), '..'))
import colors
import header
import triangular

matplotlib.rc('font', **{'family': 'serif', 'serif': ['Computer Modern Roman'],
                         'size': 8})
matplotlib.rc('text', usetex=True)
matplotlib.rc('text.latex', preamble=r'\usepackage{amsmath}'
                + r'\usepackage{braket}' + r'\usepackage{amssymb}'
                + r'\newcommand{\bvec}[1]{\boldsymbol{#1}}')

fig_width = 3.4039*2
fig_height = fig_width/2

fig, axs = plt.subplots(1, 3, figsize=(fig_width, fig_height), sharey=True,
                        gridspec_kw={'wspace': 0.05})

# TODO make this a list of four directories with data to be used
data_dirs = [
    './data_for_jacob/sc_amp_0.050/n_0.500_alpha_0.000/',
    './data_for_jacob/sc_amp_0.050/n_0.400_alpha_0.000/',
    #  './data_for_jacob/bigg/n_0.400/alpha_0.100/',
     './data_for_jacob/sc_amp_0.050/n_0.400_alpha_0.350/'
        ]

for ii, ddir in enumerate(data_dirs):
    kpts = np.load(ddir + 'k_points.npy')
    left_ = np.load(ddir + 'left.npy')
    right_ = np.load(ddir + 'right.npy')
    energy = np.load(ddir + 'ribbon_energies.npy')

    weight = 0.9

    if ii == 1:
        left = right_ > weight
        right = left_ > weight
    else:
        left = left_ > weight
        right = right_ > weight

    bulk = np.logical_and(np.logical_not(right), np.logical_not(left))

    if np.shape(kpts)[1] != np.shape(left)[1]:
        kpts = kpts[:,:np.shape(left)[1]]

    kpts_bulk = kpts[bulk] #ma.masked_array(kpts, bulk)
    energy_bulk = energy[bulk] #ma.masked_array(energy, np.logical_not(bulk)

    kpts_left = kpts[left] #ma.masked_array(kpts, np.logical_not(left))
    energy_left = energy[left] #ma.masked_array(energy, np.logical_not(left))

    kpts_right = kpts[right] #ma.masked_array(kpts, np.logical_not(right))
    energy_right = energy[right] #ma.masked_array(energy, np.logical_not(right))

    nkpts = int(np.shape(kpts)[0])
    min_bulk = np.empty(nkpts)
    energy_ = np.copy(energy)
    energy_[np.abs(energy_)< 1e-3] = 10

    for i in range(nkpts):
        min_em_ = np.amin(np.abs(energy_[i,:][bulk[i,:]]))
        min_bulk[i] = min_em_
    # min_bulk = np.reshape(np.amin(np.abs(energy_bulk),axis=1),(np.shape(kpts)[0]))
    max_bulk = -1*min_bulk
    # Plot parameters fixed here
    axs[ii].scatter(kpts_bulk, energy_bulk, c=colors.gray, s=0.02 )
    # axs[ii].fill_between(kpts[:,0],min_bulk,np.ones(nkpts),
    #                      color=colors.gray, alpha=0.5, linewidth=0.0)
    # axs[ii].fill_between(kpts[:,0],max_bulk,-1*np.ones(nkpts),
    #                      color=colors.gray, alpha=0.5, linewidth=0.0)
    axs[ii].scatter(kpts_left, energy_left, c=colors.first, s=0.3)
                #  marker='.', markersize=0.1)
    axs[ii].scatter(kpts_right, energy_right, c=colors.third, s=0.3)
                #  marker='.', markersize=0.1)

    gap_lower_edge = -5e-2 #np.min([np.min(energy_left), np.min(energy_right)])
    gap_upper_edge = 5e-2 #np.max([np.max(energy_left), np.max(energy_right)])

    axs[ii].set_xlim([-np.pi, np.pi])

    # Label the axes
    axs[ii].text(0.03, 0.98, ['(a)', '(b)', '(c)', '(d)'][ii],
                 ha = 'left', va='top',
                 transform=axs[ii].transAxes,
                 c='k', zorder=3)

    # Fix x axis
    axs[ii].tick_params(left=False, labelleft=False, bottom=True, labelbottom=True)
    if ii == 0:
        axs[ii].set_xticks([-np.pi, -0.5 * np.pi, 0, 0.5 * np.pi, np.pi],
                            labels=[r'$-\pi$', r'$-\frac{\pi} 2$', r'$0$',
                                    r'$\frac{\pi} 2$', r'$\pi$'])
    else:
        axs[ii].set_xticks([ -0.5 * np.pi, 0, 0.5 * np.pi, np.pi],
                            labels=[r'$-\frac{\pi} 2$', r'$0$',
                                    r'$\frac{\pi} 2$', r'$\pi$'])
    axs[ii].set_xlabel(r'$k/a$',labelpad=-1)

    axs[ii].set_ylim([np.amin(energy_bulk)-0.25, np.amax(energy_bulk)+0.25])

# Fix y axes
axs[0].tick_params(left=True, labelleft=True)
                 #   bottom=False, labelbottom=False)
# if ii == 0:
# axs[ii].ticklabel_format(style='sci', axis='y', scilimits=(-3,0))
# axs[ii].set_yticks([gap_lower_edge,0,gap_upper_edge], labels=[r'$-0.05$','0',r'$0.05$'])
# else:
#     axs[ii].set_yticks([gap_lower_edge,0,gap_upper_edge],
#                        labels=[r'$-5$',0,r'$5$'])
axs[0].set_ylabel(r'$E/t$') #, labelpad=-10)

fig.savefig('full_ribbons.png', bbox_inches="tight", dpi=600)
